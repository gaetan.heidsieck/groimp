member.member = member
member.members = members
member.variable = variable
member.variables = variables
member.field = field
member.fields = fields
member.method = method
member.methods = methods
member.constructor = constructor
member.constructors = constructors
member.pattern = pattern
member.patterns = patterns
member.type = type
member.types = types
member.package = package
member.packages = packages
member.two-singulars = {0} or {1}
member.two-plurals = {0} or {1}
member.three-singulars = {0}, {1}, or {2}
member.three-plurals = {0}, {1}, or {2}
member.two-members = {0} and {1}
member.add-member = {0}, {1}
member.three+-members = {0}, {1}, and {2}

compiler.no-member-in-type = No {0} named {1} was found in type {2}.
compiler.no-member-in-package = No {0} named {1} was found in package {2}.
compiler.no-member-in-scope = No {0} named {1} was found.

compiler.non-static-members = The {0} {1} {2,choice,1#is|1<are} not static.
compiler.non-instance-members = The {0} {1} {2,choice,1#is|1<are} static.
compiler.ambiguous-members = There is an ambiguity between the {0} {1}.
compiler.shadowed-members = The {0} {1} {2,choice,1#is|1<are} shadowed here.
compiler.inaccessible-members = The {0} {1} {2,choice,1#is|1<are} inaccessible here.
compiler.hidden-members = The {0} {1} {2,choice,1#is|1<are} not visible here.
compiler.inapplicable-members = The {0} {1} {2,choice,1#is|1<are} not applicable for the arguments {3}.
compiler.inapplicable-invisible-members = The {0} {1} {2,choice,1#is|1<are} neither accessible nor visible.

compiler.this-in-static-context = "this" may not be used in a static context.
compiler.super-in-static-context = "super" may not be used in a static context.
compiler.nonstatic-in-static-context = The {0} {1} is not static, and cannot be accessed in this static context.
compiler.no-enclosing-instance = No enclosing instance of type {0} was found.
compiler.no-enclosing-instance-for-member = No enclosing instance of type {2} for the {0} {1} was found.
compiler.qualified-new-of-static = A qualified new expression cannot be used for the static class {0}.
compiler.qualified-super-of-static = A qualified superclass constructor invocation cannot be used for the static class {0}.
compiler.static-member-in-inner = An inner class cannot declare static members.
compiler.abstract-in-concrete = A concrete class cannot declare abstract methods.
compiler.nonfinal-local = The local variable {0} is not final and cannot be used here.
compiler.assignment-to-final = Assignment of value to final variable {0} is not allowed.
compiler.use-before-declaration = The use of the field {0} before its declaration is not allowed here.
compiler.expr-needs-block = This kind of expression is not allowed here.
compiler.annotation-const-expected = A compiletime constant expression or a class literal is expected here.
compiler.element-not-initialized = The annotation element {0} has to be associated with a value.
compiler.no-element = No annotation element named {0} was found.
compiler.overloaded-element = {1} is not a valid annotation type: The annotation element {0} is overloaded, 

compiler.name-not-canonical = The name {0} is not the canonical name of {1}.

compiler.top-level-type-conflict = A type named {0} has already been declared or imported before.

compiler.illegal-superclass = The type {0} cannot be used as superclass.
compiler.class-extends-interface = {0} is an interface, only classes can be used here.
compiler.no-interface-type = {0} is not an interface type, only interfaces can be used here.
compiler.class-extends-final = The final class {0} cannot be used as superclass.

compiler.mismatched-constructor-name = The name {0} of the constructor does not match the name {1} of the class.

compiler.duplicate-parameter-declaration = Duplicate declaration of parameter {0}.
compiler.duplicate-local-declaration = Duplicate declaration of local variable {0}.
compiler.duplicate-field-declaration = Duplicate declaration of a field named {0}.
compiler.duplicate-constructor-declaration = Duplicate constructor declaration.
compiler.duplicate-method-declaration = Duplicate declaration of a method named {0}.
compiler.duplicate-type-declaration = Duplicate declaration of a type named {0}.
compiler.duplicate-label-declaration = Duplicate declaration of label {0}.
compiler.duplicate-element-initialization = Duplicate initialization of annotation element {0}.

compiler.cannot-override-final = The final method {0} declared in {1} cannot be overriden.
compiler.conflicting-return-type = The return type must match the return type {0} of the method {1} declared in {2}.
compiler.weaker-access-privileges = The access privileges of the method {0} declared in {1} cannot be reduced.

compiler.public-class-expected = The class {0} is not public, and cannot be used in this context where a public class is expected.
compiler.public-member-expected = The {0} {1} is not public, and cannot be used in this context where a public {0} is expected.
compiler.concrete-class-expected = The class {0} is abstract, and cannot be used where a concrete class is expected.
compiler.no-default-constructor = The class {0} does not declare a no-arg constructor.
compiler.default-constructor-not-public = The class {0} does not declare a public no-arg constructor.
compiler.instantiation-exception = An exception of class {1} occured while instantiating the class {0}: {2}.

compiler.lhs-variable = The left-hand side of an assignment must be a variable.
compiler.lhs-xl-field = The left-hand side of a parallel assignment operator must be an XL field.
compiler.inc-variable = A variable of numeric type is expected as an operand of an increment or decrement operator.
compiler.implicit-void-var = An implicitly declared variable cannot be of type void.

compiler.arrayinit-for-nonarray = An array initializer cannot be used to initialize a variable of type {0}.
compiler.incompatible-types = The type {0} is not compatible with the type {1}.
compiler.illegal-assignment-conversion = The type of this expression, {0}, is not assignable to type {1}.
compiler.illegal-return-conversion = The type {0} of the return expression does not match the return type {1} of the method.
compiler.ambiguous-conversion = There is an ambiguity between the conversions {0}.
compiler.unexpected-type = The type of this expression, {0}, is not assignable to {1}.
compiler.not-numeric = The type of this expression, {0}, is not numeric.
compiler.illegal-unop-type = Operator {0} cannot be applied to {1}.
compiler.illegal-binop-type = Operator {0} cannot be applied to {1}, {2}.
compiler.illegal-switch-type = The type of this switch expression, {0}, is not byte, short, char, or int.
compiler.illegal-label-type = The type of this expression, {0}, is not assignable to the type of the switch expression, {1}.
compiler.wrong-operand-number-for-op = Wrong number of parameters in declaration of operator method.
compiler.int-expected-for-dummy = The dummy parameter of postfix operator methods has to be of type int.
compiler.ambiguous-operator-overload = Ambiguous definition of operator overload (static and member operator).
compiler.no-reference-type = The type {0} is not a reference type.
compiler.no-array-type = The type {0} is not an array type.
compiler.no-annotation-type = The type {0} is not an annotation type.
compiler.empty-type-intersection = The intersection of the types {0} and {1} is empty.
compiler.abstract-instantiation = The abstract class {0} cannot be instantiated.
compiler.abstract-method-invocation = The abstract method {0} cannot be invoked directly.

compiler.never-an-instance = The type of the left sub-expression, {0}, cannot possibly be an instance of type {1}.
compiler.illegal-cast = An expression of type {0} cannot be cast into type {1}.

compiler.label-not-constant = A case expression must be a constant expression.
compiler.duplicate-default-label = At most one default label may be used for a switch statement. 
compiler.duplicate-switch-label = This value has already been used as label in this switch statement.
compiler.no-label-in-scope = No label named {0} was found.
compiler.no-break-target = A break statement must be enclosed in a switch, while, do or for statement.
compiler.no-continue-target = A continue statement must be enclosed in a while, do or for statement.
compiler.nonloop-continue-target = The target of a continue statement must be a while, do or for statement.

compiler.return-outside-method = A return statement must be contained within a method declaration.
compiler.iterating-nonvoid-return = A return statement within the declaration of an iterating method cannot return a value.
compiler.noniterating-yield = A yield statement must be contained within the declaration of an iterating method.
compiler.nonvoid-return = A return statement with expression must be contained within the body of a method that is declared to return a value.
compiler.void-return = A return statement with no expression may not appear within the body of a method that is declared to return a value.
compiler.nonvoid-yield = A yield statement with expression must be contained within the body of a method that is declared to return a value.
compiler.void-yield = A yield statement with no expression may not appear within the body of a method that is declared to return a value.

compiler.incompatible-modifiers = The modifiers {0} and {1} may not be combined here.
compiler.duplicate-modifier = Duplicate specification of the modifier {0}.
compiler.illegal-modifier = The modifier {0} is not allowed here.
compiler.duplicate-access-modifier = Duplicate specification of an access modifier.
compiler.redundant-modifier = The use of the {0} modifier in this context is redundant and strongly discouraged as a matter of style.
compiler.duplicate-annotation = Duplicate annotation of type {0}.
compiler.no-classpath-annotation = This annotation references classes declared in source files.

compiler.illegal-int = The value of an int literal must be a decimal value in the range -2147483648..2147483647 or a hexadecimal or octal literal that fits in 32 bits.
compiler.illegal-long = The value of a long literal must be a decimal value in the range -9223372036854775808L..9223372036854775807L or a hexadecimal or octal literal that fits in 64 bits.
compiler.illegal-float = This literal of type float has an illegal number format.
compiler.illegal-double = This literal of type double has an illegal number format.

compiler.nonnull-typeconst-expected = A non-null type constant is expected here.
compiler.wrappable-type-expected = The type {0} cannot be wrapped.
compiler.single-argterm-for-type-expected = A single argument term is expected for this wrapped type pattern.
compiler.label-for-void = A label may not be used for a void expression.

compiler.no-iteration-target = This iterator expression is not properly enclosed by an iteration target.

compiler.pattern-not-traversable = This pattern is not traversable.
compiler.closed-pattern-not-traversable = A pattern with closed ends is not traversable.
compiler.conflicting-join-terms = This pattern cannot be joined with the preceding pattern because of conflicting join terms.
compiler.dangling-out-term = This pattern has a dangling out term.
compiler.dangling-in-term = This pattern has a dangling in term.
compiler.missing-parent-for-branch = A parent for this branch is missing.
compiler.out-term-of-parent-not-closed = The parent of this branch does not have a closed out term.
compiler.pattern-not-labelable = Only node pattern can be labelled.
compiler.duplicate-place-label = This place has already been labelled as {0}.
compiler.qvar-expected = {0} is not a query variable. 
compiler.qvar-at-higher-level = {0} is a query variable at higher level.

compiler.no-production-context = There is no production context at this point.
compiler.void-producer = This produce statement returns void. 
compiler.no-query-model = No compile-time model for queries has been set.
compiler.no-query-model-in-graph = No compile-time model for queries has been set in type {0}.

compiler.no-instantiation-producer-type = No enclosing annotation @InstantiationProducerType.

compiler.declaration-hides-field = This declaration hides the field {0} declared in superclass {1}.
compiler.deprecated-member = The {0} {1} is deprecated and should no longer be used.

compiler.unimplemented-method = The type {0} must implement the method {1}.