package de.grogra.projectmanager;

import java.io.IOException;
import java.util.HashMap;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.registry.Plugin;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Project;
import de.grogra.pf.ui.ProjectManager;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.ProjectWorkbenchLauncher;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.util.Map;
import de.grogra.xl.util.ObjectList;

public class ProjectManagerImpl extends Plugin implements ProjectManager {
	private static ProjectManagerImpl PLUGIN;
	private static int projectId; // next id for new project.
	
	/*
	 * Map of Project and a list of workbench linked to it. 
	 */
	private final java.util.Map<Project, ObjectList<Workbench>> projects = new HashMap<Project, ObjectList<Workbench>> ();

	
	public ProjectManagerImpl() {
		if(PLUGIN == null) {
			PLUGIN = this;
		}
		projectId = 0; // for the naming of the Project directories in the Registry
	}

	public static ProjectManager getInstance() {
		return PLUGIN;
	}

	/**
	 * Create a new directory for the Project and add a reference to 
	 * the project as the first entry.
	 * @param ep
	 * @return Project Id (also name of the directory)
	 */
	private Object registerProject(Project ep) {
		synchronized (projects)
		{
			projects.put (ep, new ObjectList<Workbench>(1, false));
		}
		return projectId++;
	}

	/**
	 * Close the project.
	 * @param projectId
	 */
	private void unregisterProject(Project project) {
		synchronized (projects)
		{
			projects.remove(project);
			project.dispose();
		}
	}

	/**
	 * Add a workbench to a reference to a workbench to the project 
	 * directory. 
	 * The reference is named after the application type, meaning that 
	 * each UIApplication can only register one workbench per Project
	 * @param projectId
	 * @param client
	 */
	public void linkWorkbenchToProject(Project project, ProjectWorkbench client) {
		synchronized (projects)
		{
			ObjectList<Workbench> w = projects.get(project);
			if (w == null) {
				throw new IllegalStateException ("The project is not registered.");
			}
			if (w.contains(client)) {
				throw new IllegalStateException ("Workbench already registered.");
			}
			w.add(client);
		}
	}

	/**
	 * Unlink the client workbench. If the project has no other workbench open, close it.
	 * @param project
	 * @param client
	 */
	private void unlinkWorkbenchFromProject(Project project, ProjectWorkbench client) {
		synchronized (projects)
		{
			ObjectList<Workbench> w = projects.get(project);
			if (w == null) {
				throw new IllegalStateException ("The project is not registered.");
			}
			if (!w.contains(client)) {
				throw new IllegalStateException ("Workbench not linked to project.");
			}
			w.remove(client);
			if (w.size == 0) {
				unregisterProject(project);
			}
		}
	}
	/**
	 * - Call a ProjectFactory to create a new ExecutableProject 
	 * based on a child registry.
	 * - Register this new Project to the manager and than call 
	 * the UIApplication to create a new Workbench around the project 
	 * - Return the Workbench to the Project.
	 * 
	 * The basic idea of the createNewWorkbench function is that a 
	 * "Naked" Project should not leave the ProjectManager
	 * @throws IOException 
	 */
	@Override
	public Project createProject(ProjectFactory projectFactory) throws IOException {
		Project ep = projectFactory.createProject(Registry.create(getRegistry()), PLUGIN);
		Object projectId = registerProject(ep);
		ep.setId(projectId);
		return ep;
	}
	
	@Override
	public Project createProject(ProjectFactory projectFactory, Registry r) throws IOException {
		Project ep = projectFactory.createProject(r, PLUGIN);
		Object projectId = registerProject(ep);
		ep.setId(projectId);
		return ep;
	}

	public Project openProject(ProjectFactory projectFactory, FilterSource fs,
			Map initParams) throws IOException {
		//create Registries for the Project and the Workbench
		Registry projectRegistry = Registry.create(getRegistry());

		//load the Project and with it the Registry and the Graph
		Project ep = projectFactory.loadProject(fs, initParams,projectRegistry , PLUGIN);
		Object projectId = registerProject(ep);
		ep.setId(projectId);
		return ep;
	}
	

	/**
	 * get the Id of the Project
	 */
	@Override
	public Object getProjectId(Project ep) {
		return ep.getId();
	}

	@Override
	public Project getProject(Object id) {
		synchronized (projects)
		{
			for (Project p : projects.keySet()) {
				if (p.getId().equals(id)) {
					return p;
				}
			}
		}
		return null;
	}

	
	/**
	 * create a new Workbench around the selected Project and 
	 * register the Workbench 
	 * It pushes the registry 's project into the wb
	 */
	@Override
	public ProjectWorkbench connectToProject(Object id, ProjectWorkbenchLauncher wbLauncher) {
		Project ep = getProject(id);
		ProjectWorkbench wb =wbLauncher.create(ep);
		linkWorkbenchToProject(ep, wb);
		return wb;
	}

	/**
	 * Unlink workbench from a project
	 */
	@Override
	public void disconnectProject(ProjectWorkbench client) {
		unlinkWorkbenchFromProject(client.getProject(), client);
	}

	
	/**
	 * List of open project name.
	 */
	@Override
	public String[] listOpenProjects() {
		synchronized (projects)
		{
			String[] list = new String[projects.size()];
			int i = 0;
			for (Project p : projects.keySet()) {
				list[i] = p.getId() + " : "+ p.getName();
			}
			return list;
		}
	}

	
	/**
	 * Find all opened workbench linked to a project.
	 */
	@Override
	public ProjectWorkbench[] getLinkedWorkbenches(Project ep) {
		synchronized (projects)
		{
			ObjectList<Workbench> w = projects.get(ep);
			if (w == null) {
				return new ProjectWorkbench[0];
			}
			return w.toArray(new ProjectWorkbench[0]);
		}
	}
	
}
