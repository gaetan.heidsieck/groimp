package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;

import de.grogra.pf.registry.Item;

/**
 * Item that enables a UITransferHandler to know what type of 
 * dataflavor the panel can import.
 */
public abstract class UIDataHandler extends Item {
	
	String[] flavors;
	//enh:field
	
	private transient DataFlavor[] flavorsObj;
	private transient boolean init = false;
	
	public UIDataHandler() {
		super(null);
	}
	public UIDataHandler(String key) {
		super(key);
	}

	public DataFlavor[] getDataFlavors() {
		if (!init) { loadFlavors(); }
		return flavorsObj;
	}
	
	void loadFlavors() {
		try {			
			flavorsObj = new DataFlavor[flavors.length];
			for (int i=0; i<flavors.length; i++) {
				flavorsObj[i] = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType +
					        ";class=\"" +
					        flavors[i] +
					        "\"", null, getPluginDescriptor().getPluginClassLoader());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Test if the remote flavor can be cast on one of the accepted flavor
	 */
	public boolean isDataFlavorSupported(DataFlavor f) {
		for (DataFlavor d : getDataFlavors()) {
			if(d.getRepresentationClass().isAssignableFrom(f.getRepresentationClass()) ) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isDataFlavorSupported(DataFlavor[] flav) {
		for (DataFlavor f : flav) {
			if (isDataFlavorSupported(f)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get the flavor that support the incoming flav
	 */
	public DataFlavor getDataFlavorSupported(DataFlavor f) {
		for (DataFlavor d : getDataFlavors()) {
			if(d.getRepresentationClass().isAssignableFrom(f.getRepresentationClass()) ) {
				return d;
			}
		}
		return null;
	}
	public DataFlavor getDataFlavorSupported(DataFlavor[] flav) {
		for (DataFlavor f : flav) {
			if (isDataFlavorSupported(f)) {
				return getDataFlavorSupported(f);
			}
		}
		return null;
	}
	

	abstract public Object loadData(Object t);
	
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field flavors$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (UIDataHandler.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((UIDataHandler) o).flavors = (String[]) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((UIDataHandler) o).flavors;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (UIDataHandler.class);
		$TYPE.addManagedField (flavors$FIELD = new _Field ("flavors", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String[].class), null, 0));
		$TYPE.validate ();
	}

//enh:end
	
	
	@Override
	protected boolean readAttribute (String uri, String name, String value)
		throws org.xml.sax.SAXException
	{
		if ("".equals (uri) && "flavor".equals (name))
		{
			flavors = new String[] {value};
			return true;
		}
		return super.readAttribute (uri, name, value);
	}
}
