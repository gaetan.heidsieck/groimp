package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

/**
 * Base interface of Object transferable through the UI. These objects
 * are handled by a UITranferHandler.
 */
public interface UITransferable extends Transferable {

	public final DataFlavor SOURCE = new DataFlavor("application/source;class=\"java.lang.Object\"", null);
	public final DataFlavor DISPLAYNAME = new DataFlavor("application/displayname;class=\"java.lang.Object\"", null);
	
	public Object getSource();
	public Object getContent();
	public String getDisplayName();
}
