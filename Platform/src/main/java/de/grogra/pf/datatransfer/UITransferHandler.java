package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.List;

/**
 * Default interface for TranferHandler in the UI. Implements some of the
 * swing TranferHandler (but not all - the transfer handler could be 
 * implemented for headless/ api UI).
 */
public interface UITransferHandler {

	public void setDataHandlers(List<UIDataHandler> d);
	public DataFlavor[] getDataFlavors();
	public UITransferable createTransferable(Object source);
	public List<UIDataHandler> getHandlers();
	 /**
	 * Find the most suited handler and use it to load the data.
	 * Can be overridden for more specific loading.
	 */
	default public Object loadData(Object t) {
		if (t instanceof Transferable) {
			UIDataHandler h = getMostSuitedHandler((Transferable)t);
			if (h != null) {
				return h.loadData(t);
			}
		}
		return null;
	}
	
	default public UIDataHandler getMostSuitedHandler(Transferable t) {
		UIDataHandler best = null;
		for (UIDataHandler h : getHandlers()) {
			if (h.isDataFlavorSupported(((Transferable)t).getTransferDataFlavors())) {
				if (best == null) {
					best = h;
				} else {
					DataFlavor dbest = best.getDataFlavorSupported(((Transferable)t).getTransferDataFlavors());
					DataFlavor nbest = h.getDataFlavorSupported(((Transferable)t).getTransferDataFlavors());
					best = (DataFlavorComparator.COMPARATOR.compare(dbest, nbest) > 0) ? best : h;
				}
			}
		}	
		return best;
	}
}
