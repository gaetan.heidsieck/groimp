/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.itextpdf.layout.Document;
import com.itextpdf.commons.exceptions.ITextException;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfVersion;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.kernel.colors.ColorConstants;

import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.OutputStreamSource;
import de.grogra.util.MimeType;

public class PDFWriter extends FilterBase implements OutputStreamSource {
	
	public static final IOFlavor FLAVOR = new IOFlavor (MimeType.PDF, IOFlavor.OUTPUT_STREAM, null);

	public PDFWriter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(FLAVOR);
	}

	public void write(OutputStream out) throws IOException {
		Dataset ds = (Dataset) ((ObjectSource) source).getObject();
		ByteArrayOutputStream baos = null;
		int column = ds.getColumnCount();
		int row = ds.getRowCount();
		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(baos,
				new WriterProperties()
                .addXmpMetadata()
                .setPdfVersion(PdfVersion.PDF_1_6)));
		try {
		    PdfDocumentInfo info = pdfDoc.getDocumentInfo();
		    info.setAuthor("GroIMP");  
		    info.setSubject(ds.getTitle());
		    info.setTitle(ds.getTitle());
		    info.setCreator("GroIMP");
		    info.setKeywords(ds.getTitle());
		    
		    // In iText7 the footer have to be added as an event. 
//			Paragraph footer = new Paragraph("Created by GroIMP                   " +ds.getTitle()+
//					"                                        Seite "));
//			footer.setAlignment(HeaderFooter.ALIGN_CENTER);
//			footer.setBorderWidth(0);
//			footer.setBorderWidthTop(1);
// 			document.setFooter(footer);

			Document document = new Document(pdfDoc, PageSize.A4.rotate());
			
			int[] widths = new int[column];
			for(int i = 0; i < column; i++)
				widths[i] = 10;
			
			Table table=new Table(UnitValue.createPercentArray(column));
			table.setWidth(UnitValue.createPercentValue(100));
//			table.setWidths(widths);
//			table.getDefaultCell().setBorderWidth(1);
			table.setBackgroundColor(ColorConstants.LIGHT_GRAY);
			table.setTextAlignment(TextAlignment.CENTER);
//			table.getDefaultCell().setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			Paragraph par;
			
			for(int i = 0; i < column; i++) {
				table.addCell(par=new Paragraph(ds.getColumnKey(i).toString()));
				par.setFontSize(9);
			}
			
			table.addHeaderCell("");
			table.setBackgroundColor(ColorConstants.WHITE);
			table.setTextAlignment(TextAlignment.LEFT);
			
			for(int i = 0; i < row; i++) {
				for(int j = 0; j < column; j++) {
					table.addCell(par=new Paragraph(String.valueOf(ds.getCell(i, j).getX())));
					par.setFontSize(9);
				}
			}
			
			document.add(table);
			document.close();
			baos.writeTo(out);
			baos.close();
		} catch (ITextException e) {
			e.printStackTrace();
		}
	}

}
