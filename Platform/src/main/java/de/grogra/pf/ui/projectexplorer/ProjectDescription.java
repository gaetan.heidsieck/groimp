package de.grogra.pf.ui.projectexplorer;

import de.grogra.util.Described;

import javax.swing.*;

/**
 * This interface add some described parameters for project directories.
 * The described items are used for displaying, parsing and opening a
 * list of projects descriptions. Obtained, for instance, by querying the
 * gallery or the set of local examples.
 */
public interface ProjectDescription extends Described {

    /**
     * Constant for {@link #getDescription(String)} specifying the
     * tags. The returned value has to be a <code>String</code>.
     */
    String TAGS = "Tags";

    /**
     * Constant for {@link #getDescription(String)} specifying an image
     */
    String IMAGE = "Image";

    /**
     * Constant for {@link #getDescription(String)} specifying some authors.
     */
    String AUTHORS = "Authors";

    /**
     * Constant for {@link #getDescription(String)} specifying the category.
     */
    String CATEGORY = "Category";


    /**
     * Constant for {@link #getDescription(String)} specifying the Project name.
     */
    String PROJECT_NAME = "ProjectName";
}
