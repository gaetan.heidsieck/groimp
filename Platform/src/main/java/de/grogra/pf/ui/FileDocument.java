package de.grogra.pf.ui;

import javax.swing.text.Document;

import de.grogra.util.MimeType;


/**
 * A document that is opened by the text editor. It is linked to a FileSource to 
 * push the modifications in.
 */
public interface FileDocument extends Document {
	public boolean isModified();
	public boolean isEditable();
	public String getSystemId();
	public MimeType getMimeType();
	public void save();
}
