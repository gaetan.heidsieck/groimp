package de.grogra.pf.ui.autocomplete;

public interface TextAreaListener {

	public void fileChanged(TextAreaEvent e);

	public void insertUpdate(TextAreaEvent e);
	public void removeUpdate(TextAreaEvent e);
	public void changedUpdate(TextAreaEvent e);
}
