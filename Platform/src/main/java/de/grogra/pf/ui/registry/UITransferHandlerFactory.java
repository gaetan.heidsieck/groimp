package de.grogra.pf.ui.registry;

import de.grogra.pf.datatransfer.UITransferHandler;
import de.grogra.pf.datatransfer.UITransferHandlerImpl;
import de.grogra.pf.registry.Item;

public class UITransferHandlerFactory extends Item {

	public UITransferHandlerFactory() {
		super(null);
	}
	public UITransferHandlerFactory(String key) {
		super(key);
	}

	public UITransferHandler createHandler() {
		return new UITransferHandlerImpl();
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new UITransferHandlerFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new UITransferHandlerFactory ();
	}

//enh:end
}
