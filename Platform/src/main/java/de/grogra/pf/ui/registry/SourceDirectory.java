
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import java.io.IOException;

import javax.swing.event.TreeModelEvent;
import de.grogra.pf.io.IO;
import de.grogra.pf.registry.*;
import de.grogra.pf.ui.*;

public final class SourceDirectory extends Item
{

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new SourceDirectory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new SourceDirectory ();
	}

//enh:end


	private SourceDirectory ()
	{
		this (null, null);
	}


	public SourceDirectory (String key, String systemId)
	{
		super (key);
		this.systemId=systemId;
		setDirectory ();
	}


	public Item getResourceDirectory ()
	{
		Item p = (Item) getAxisParent ();
		if (p instanceof SourceDirectory) {
			return ((SourceDirectory) p).getResourceDirectory ();
		}
		return p;
	}
	
	
	public String getBaseName ()
	{
		String s = (String) getDescription ("Base");
		if (s == null)
		{
			s = getName ();
			if (s.endsWith ("s"))
			{
				s = s.substring (0, s.length () - 1);
			}
		}
		return s;
	}
	
	
	public static SourceDirectory get (Item child)
	{
		while ((child != null) && !(child instanceof SourceDirectory))
		{
			child = (Item) child.getAxisParent ();
		}
		return (SourceDirectory) child;
	}
	
	@Override
	protected Object getDescriptionImpl (String type)
	{
		if (NAME.equals (type))
		{
			return IO.toName (getName ());
		}
		return super.getDescriptionImpl (type);
	}
	
	@Override
	public void addRequiredFiles (java.util.Collection list)
	{
		Object f = getRegistry ().getProjectFile ((String)getSystemId ());
		if (f != null)
		{
			list.add (f);
		}
	}
	
	@Override
	public void delete() {
		try {
			getRegistry ().getFileSystem ().delete(getRegistry().getProjectFile((String)getSystemId()));
			// remove from parent (only iff all children removed && delete OK)
		} catch (IOException e) {
			Workbench.get (this).logGUIInfo (
					IO.I18N.msg ("deletefile.failed", getName ()), e);
		}
	}
	
	@Override
	protected void activateImpl ()
	{
		// for backward compatibility
		IO.setSystemIdFromName(this);
		getRegistry ().getProjectFile ((String)getSystemId ());
	}

	@Override
	protected void deactivateImpl ()
	{
		
	}
	
	public void treeNodesInserted (TreeModelEvent e)
	{
		// add children here
	}
	
	public void treeNodesRemoved (TreeModelEvent e)
	{
		// remove children here
	}

	public void treeNodesChanged (TreeModelEvent e)
	{
	}

	public void treeStructureChanged (TreeModelEvent e)
	{
		//save the project when structure changed or directory added
	}


}
