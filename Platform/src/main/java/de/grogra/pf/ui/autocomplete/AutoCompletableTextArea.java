package de.grogra.pf.ui.autocomplete;

import java.awt.ComponentOrientation;
import java.beans.Transient;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Highlighter;
import javax.swing.text.Position;
import javax.swing.text.Segment;

import de.grogra.pf.ui.autocomplete.impl.CompletionProvider;

import java.awt.Point;

/**
 * Default interface that textarea needs to implements to define the required methods for 
 * autocompletion
 * The classes that implements this interface MUST extends JComponent
 * 
 */
public interface AutoCompletableTextArea {

	/**
	 * Initializes this completion provider.
	 */
	public void initCompletionProvider();
	
	/**
	 * Returns the text just before the current caret position that could be
	 * the start of something auto-completable.<p>
	 */
	public String getAlreadyEnteredText(CompletionProvider provider);
	
	/**
	 * Used by the AbstractAutoCompletion object to bind its keys action
	 */
	public InputMap getInputMap();
	public ActionMap getActionMap();
	
	/**
	 * Required text methods to check and modify the text buffer
	 */
    
	/**
	 * Return the position of the caret as int.
	 */
	public int getCaretPosition();
	
	/**
	 * Set the position of the caret to dot
	 */
	@Transient
	public void setCaretPosition(int dot);

	/**
	 * Return the line of the caret.
	 */
	public int getLineOfCaret();
	
	/**
	 * Select the text between the two position.
	 */
	public void selectText(int start, int end);

	/**
	 * Replace the current selection by the String content
	 * @param content 
	 */
	public void replaceSelection(String content);
	
	/**
	 * Return the text at position start of given length
	 */
	public String getText(int start, int length) throws BadLocationException;

	/**
	 * Push the text at position start of given length in the given segment
	 */	
	public void getText(int start, int end, Segment s) throws BadLocationException;

	/**
	 * Return the end offset of the current selection
	 */
	@Transient
    public int getSelectionEnd();
	/**
	 * Return the start offset of the current selection
	 */
	@Transient
    public int getSelectionStart();

	/**
	 * Create the position at the position lenght. If length is bigger than the buffer
	 * size it will throw an error.
	 */
	public Position createPosition(int length) throws BadLocationException;
	
	/**
	 * Move the caret to the given position
	 */
	public void moveCaretPosition(int pos);
	public void moveCaretPositionFromXY(int x, int y);
	
	/**
	 * Get the XY position of the caret in pixels
	 */
	public Point getXYCaretPosition();
	
	/**
	 * Return the orientation of the component. 
	 */
	public ComponentOrientation getComponentOrientation();
	
	/**
	 * Listeners
	 */
	public void addCaretListener(CaretListener listener); // changes in the caret location
	public void removeCaretListener(CaretListener listener);
	public void addTextAreaListener(TextAreaListener listener); // changes in the text
	public void removeTextAreaListener(TextAreaListener listener);
	
	/**
	 * Return the Highlighter object of the text area
	 */
	public Highlighter getHighlighter();
	
	/**
	 * Return the buffer size of the text area
	 */
	public int getDocumentLength();

	/**
	 * Return the Text contains between the begining of the given line and the caret position.
	 * It suppose that the caret position is after the start of the line 
	 * @return
	 */
	public String getBeginLineText(int lineNb);
}
