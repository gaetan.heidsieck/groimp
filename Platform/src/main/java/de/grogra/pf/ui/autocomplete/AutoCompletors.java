package de.grogra.pf.ui.autocomplete;

import java.util.prefs.Preferences;

import javax.swing.DefaultListModel;

import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Option;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.edit.EnumerationEditor;
import de.grogra.util.Described;
import de.grogra.util.StringMap;

public class AutoCompletors {

	DefaultListModel<String> list = new DefaultListModel ();
	final static StringMap allACs = new StringMap ();
	final static StringMap acsInfos = new StringMap ();


	public AutoCompletors (Registry reg)
	{
		Item item = Item.resolveItem(reg.getRootRegistry(), "/ui/autocompletors") ;
		Item opt = (Item) item.getBranch ();
		opt = Option.get (opt, "acs");
		if (item != null)
		{
			Item e = (Item) opt.getBranch ();
			if (e instanceof EnumerationEditor)
			{
				for (Item i = (Item) item.getBranch (); i != null;
						i = (Item) i.getSuccessor ())
				{
					if (i instanceof AutoCompletorFactory)
					{
						allACs.put(i.getDescription(Described.NAME), i);
						acsInfos.put(i.getName(), i.getDescription(Described.NAME));
					}
				}
				for (int i = 0; i < allACs.size (); i++)
				{
					list.addElement (allACs.getKeyAt (i));
				}
				((EnumerationEditor) e).setList (list);
			}
		}
		String curName = null;
		Preferences prefs0 = Preferences.userRoot().node(this.getClass().getName());
		if (prefs0!=null) {
			Preferences  prefs1 = prefs0.node("/de/grogra/options/ui/autocompletors/autocomp");
			if(prefs1!=null) {
				curName = prefs1.get("acs", null);
			}
		}
		if (curName != null && list.contains(curName)) {
			((Option)opt).setOptionValue(curName);
		} else {curName = null;}
		if ( (curName==null || curName.isEmpty() ) && list.size() > 0) {
			if (acsInfos.containsKey("default") ) { 
				((Option)opt).setOptionValue( acsInfos.get("default"));
			} else {
				((Option)opt).setOptionValue(list.getElementAt(0));
			}
		}
		
	}	

	public static AutoCompletorFactory getCompletorFactory(String dname) {
		return (AutoCompletorFactory) allACs.get(dname);
	}
}
