package de.grogra.pf.ui.autocomplete;

public interface TextAreaEvent {

	public enum Type {
		INSERT,
		CHANGE,
		REMOVE
	}
	/**
	 * Absolute position before the event
	 */
    public int getOffset();
    /**
     * Lenght of the modified content
     */
    public int getLength();
    /**
     * Origin of the event
     */
    public AutoCompletableTextArea getDocument();
    public Type getType();
    
    /**
     * When the document change
     */
	public AutoCompletableTextArea getOldFile(); 
	public AutoCompletableTextArea getNewFile();
}
