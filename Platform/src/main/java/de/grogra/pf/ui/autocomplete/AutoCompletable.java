package de.grogra.pf.ui.autocomplete;

import de.grogra.pf.ui.autocomplete.impl.AbstractAutoCompletor;
import de.grogra.pf.ui.autocomplete.impl.CompletionProvider;

/**
 * Interface that define the required methods for a text panel to be 
 * able to auto complete the text in a document. (The panel is managing
 * the document completion)
 */
public interface AutoCompletable {

	/**
	 * Call the auto completion
	 * Likely useless with the autocmpletor implementation
	 */
	public void autocomplete();
	
	/**
	 * Get the String to be autocompleted
 	 * Likely useless with the autocmpletor implementation
	 */
	public String getAlreadyEnteredText(CompletionProvider provider);
	
	/**
	 * Add an autocompetor to the panel
	 */
	public void installAutoCompletor(AbstractAutoCompletor ac);
	
	/**
	 * remove an autocompetor
	 */
	public void uninstallAutoCompletor();
}
