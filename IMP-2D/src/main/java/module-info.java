module imp2d {
	exports de.grogra.imp2d;
	exports de.grogra.imp2d.graphs;
	exports de.grogra.imp2d.layout;
	exports de.grogra.imp2d.objects;
	exports de.grogra.imp2d.edit;

	requires graph;
	requires imp;
	requires math;
	requires platform;
	requires platform.core;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	requires java.xml;
	
	opens de.grogra.imp2d to utilities;
}