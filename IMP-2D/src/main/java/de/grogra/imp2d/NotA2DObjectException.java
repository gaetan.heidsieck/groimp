package de.grogra.imp2d;

public class NotA2DObjectException extends Exception implements de.grogra.util.UserException {
	public NotA2DObjectException(String msg) {
		super(msg);
	}
}
