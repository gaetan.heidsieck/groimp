package de.grogra.imp2d;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import de.grogra.graph.ArrayPath;
import de.grogra.graph.Path;
import de.grogra.imp.viewhandler.ToolEventImpl;
import de.grogra.imp.viewhandler.ViewEventHandler;
import de.grogra.pf.ui.event.ClickEvent;
import de.grogra.pf.ui.event.DragEvent;

public class ToolEvent2DImpl extends ToolEventImpl {

	boolean hasSelected;
	
	public ToolEvent2DImpl(ViewEventHandler h, EventObject e) {
		super(h, e);
		hasSelected=false;
	}

	@Override
	public void eventOccured(EventObject e) {
		if (!(e instanceof MouseEvent))
		{
			return;
		}
		MouseEvent me = (MouseEvent) e;
		me.consume ();
		switch (me.getID ())
		{		
			case MouseEvent.MOUSE_RELEASED:
				handler.disposeEvent (new MouseEvent(me.getComponent(), me.getID(), me.getWhen(), me.getModifiers(),
						me.getX(), me.getY(), me.getClickCount(), me.isPopupTrigger(), me.getButton()));
				Path p = handler.getPickedTool(-1);
				if (handler.hasListener (p))
				{
					p = new ArrayPath (p);
					ClickEvent ce = createClickEvent (me);
					ce.set (handler.getView (), p);
					ce.set(new MouseEvent(me.getComponent(), MouseEvent.MOUSE_CLICKED, me.getWhen(), me.getModifiers(),
							me.getX(), me.getY(), me.getClickCount(), me.isPopupTrigger(), me.getButton()));
					handler.send (ce, p);
				}
				return;
			case MouseEvent.MOUSE_DRAGGED:
				if (handler.isMouseOnSelected(pressEvent)) {
					break;
				}
				if (!hasSelected && pressEvent!=null) {
					hasSelected=true;
					handler.selectFromEvent(new MouseEvent(me.getComponent(), MouseEvent.MOUSE_CLICKED, me.getWhen(), me.getModifiers(),
							pressEvent.getX(), pressEvent.getY(), me.getClickCount(), me.isPopupTrigger(), me.getButton()));
					handler.disposeEvent (new MouseEvent(me.getComponent(), MouseEvent.MOUSE_PRESSED, me.getWhen(), me.getModifiers(),
							pressEvent.getX(), pressEvent.getY(), pressEvent.getClickCount(), me.isPopupTrigger(), me.getButton()));
					return;
				}
		}
		super.eventOccured(e);
	}
	
	@Override
	protected void mouseDragged (MouseEvent event, int dragState, int dx, int dy)
	{
		Path p = handler.getPickedTool(0);
		if (handler.hasListener (p))
		{
			p = new ArrayPath (p);
			DragEvent me = createDragEvent (event);
			me.set (handler.getView (), p);
			me.set (event);
			me.setDragData (dragState, dx, dy);
			handler.send (me, p);
		}
	}
}
