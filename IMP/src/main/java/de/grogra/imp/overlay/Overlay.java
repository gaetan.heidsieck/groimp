package de.grogra.imp.overlay;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point2f;

/**
 * A set of Nodes that are displayed on top of a View.
 */
public class Overlay {

	transient volatile List<OverlayShape> shapes;
	
	public Overlay() {
		shapes = new ArrayList<OverlayShape>();
	}
	
	public synchronized void add(OverlayShape n) {
		if (shapes == null) {
			shapes = new ArrayList<OverlayShape>();
		}
		shapes.add(n);
	}
	public synchronized void clear() {
		shapes.clear();
	}
	public boolean isEmpty() {
		return shapes == null || shapes.size() == 0;
	}
	
	/**
	 * Test if the given node is in the shapes of the overlay
	 */
	public boolean isInShape(Point2f n) {
		for (OverlayShape s : shapes) {
			if (s.isIn(n)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * The renderer is the managing object that deal with the rendering. (RenderState in imp 3d) 
	 * @param renderer
	 */
	public synchronized void draw(Object renderer) {
		for (OverlayShape s : shapes) {
			s.draw(renderer);
		}
	}
	
	/**
	 * Get the center point of the shapes. 
	 */
	public Point2f getCenter() {
		Point2f p2=null;
		for (OverlayShape s : shapes) {
			p2 = s.getCenter(p2);
		}
		return p2;
	}
	
	/**
	 * Get radius of the shapes. aka the biggest distance between the center and the outer part of
	 * the shape.
	 */
	public float getRadius() {
		float f=-1;
		for (OverlayShape s : shapes) {
			f = s.getRadius(f);
		}
		return f;
	}
}
