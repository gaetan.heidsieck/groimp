package de.grogra.imp.overlay;

import javax.vecmath.Point2f;

/**
 * Base for objects that can be added in the overlay. The overlay represent 
 * a 2d scene. Thus the test uses 2d points.
 */
public interface OverlayShape {

	/**
	 * The renderer is the managing object that deal with the rendering. 
	 * (e.g. RenderState in imp 3d) 
	 * @param renderer
	 */
	public void draw(Object renderer);
	public boolean isIn(Point2f n);
	/**
	 * Compute the center of the based on the given p2. p2 can be null if 
	 * there is no transformation before.
	 */
	public Point2f getCenter(Point2f p2);
	/**
	 * Compute the radius of the shape. i.e. the distance from the center to 
	 * the further outer part. f can be -1 if there is no radius before.
	 */
	public float getRadius(float f);
}
