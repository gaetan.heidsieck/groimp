package de.grogra.imp.objects;

import de.grogra.graph.impl.Node;

/**
 * Interface for object that can be transformed into Node by a Producer. 
 * Every class can implements "public Node toNode()" and will be handled by 
 * a producer, but if you want to force the Producer produce a Node based
 * on an interface, this is required.
 */
public interface ProducingNode {
	public Node produceNode();
}
