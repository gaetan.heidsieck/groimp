package de.grogra.imp.viewhandler;

import de.grogra.persistence.SCOType;

public class EmptyViewEventFactory extends ViewEventFactory{

	//enh:sco SCOType

	public EmptyViewEventFactory() {
		this.navigatorFactory = new EmptyEventFactory();
		this.toolFactory = new EmptyEventFactory();
		this.selectionFactory = new EmptyEventFactory();
		this.highlighter = new EmptyHighlighter();
	}
		
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (EmptyViewEventFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new EmptyViewEventFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (EmptyViewEventFactory.class);
		$TYPE.validate ();
	}

//enh:end
}
