package de.grogra.imp.viewhandler;

import java.util.EventObject;

import de.grogra.imp.PickList;
import de.grogra.util.Disposable;
import de.grogra.pf.ui.Context;

/**
 * Base class that highlighter have to extends. It defines the base highlight methods.
 */
public abstract class Highlighter implements Disposable {

	/**
	 * Calculate the highlight for the mouse position x and y
	 * @param x
	 * @param y
	 */
	public abstract void calculateHighlight (EventObject e);
	
	/**
	 * reset current highlight
	 */
	public abstract void resetHighlight();
	
	/**
	 * Set the objects in list as highlighted
	 */
	public abstract void highlight (PickList list, int index, int[] changed);

	/**
	 * Return the highlighted index (the object poitned by ALT+Click)
	 * @return
	 */
	public abstract int getIndex();

	public abstract void init(Context ctx);
}
