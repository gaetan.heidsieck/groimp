package de.grogra.imp.viewhandler;

import java.util.EventObject;

/**
 * The <code>SelectionEvent</code> is the base for the <code>Selection</code>.
 */
public interface SelectionEvent extends de.grogra.util.DisposableEventListener{
	
	public void select(EventObject e);
}
