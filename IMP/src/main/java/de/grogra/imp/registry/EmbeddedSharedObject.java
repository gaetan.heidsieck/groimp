package de.grogra.imp.registry;

import java.io.IOException;

import de.grogra.persistence.DisposableField;
import de.grogra.persistence.Manageable;
import de.grogra.persistence.ManageableType;
import de.grogra.persistence.ShareableBase;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.pf.io.ExtensionItem;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.vfs.FileSystem;
import de.grogra.xl.util.ObjectList;

/**
 * Object that is read/write from a file but created on runtime from 
 * the project graph. Such object need to implements DisposableField to 
 * process the information when a node is removed from the graph.
 */
public abstract class EmbeddedSharedObject extends ShareableBase implements DisposableField {

	Command UNMODIFY = new Command() {
		@Override public String getCommandName() {
			return "unmodify";
		}
		@Override
		public void run(Object info, Context context) {
			context.getWorkbench().setModified(false);
		}
	};
	
	private transient SharedObjectProvider sop;
	private transient ObjectList refs = null;

	public EmbeddedSharedObject() { // 
	}

	public EmbeddedSharedObject(String name) {
		createProvider(name);
	}

	/**
	 * Manage where the file should be stored
	 */
	abstract public MimeType getMimeType();
	abstract public String getExtension();
	abstract public String getResourceDirectoryName();
	/**
	 * The java class name of the object type
	 */
	abstract public String getObjectType();
	/**
	 * True if the object is not "empty" as its persistence depends 
	 * how the mimetype write content, a java object with some 
	 * value could be defined as "empty" 
	 */
	abstract public boolean hasContent();

	@Override
	public ManageableType getManageableType() {
		return null;
	}

	@Override
	public void initProvider(SharedObjectProvider provider) {
		this.sop = provider;
	}

	@Override
	public SharedObjectProvider getProvider() {
		return getProvider(false);
	}
	
	public SharedObjectProvider getProvider(boolean create) {
		if (sop == null && create) {
			createProvider(null);
		}
		return sop;
	}

	private void createProvider(String name) {
		Registry r = Registry.current();

		Item resDir = r.getDirectory("/project/objects/"+getResourceDirectoryName(), null);
		if (resDir == null) { return; }
		MimeType mtype = getMimeType();

		if (name == null || name.isEmpty()) {

			name = "sharedObject"+(getExtension()!=null?"."+getExtension():"");
		}

		try {
			FileSystem fs = r.getFileSystem();
			Object file = fs.createIfDoesntExist(fs.getRoot(), getResourceDirectoryName(), true, false);
			file = fs.createIfDoesntExist(file, name, false, true);

			FileSource source = new FileSource(fs, file, IO.toSystemId(fs, file), mtype, r, new StringMap());

			EmbeddedFileObject soProvider = new EmbeddedFileObject(
					source.getSystemId(), getMimeType(), this, getObjectType() );
			soProvider.setObjDescribes(true);
			resDir.addUserItemWithUniqueName (soProvider, fs.getName(file));
			
			//TODO: pretty bad work around but the creation of the sop and when added it to the registry
			// this pushes the jm to set the wb to modified. 
			Workbench w = Workbench.current();
			JobManager j = w.getJobManager ();
			j.runLater(UNMODIFY, null, w, JobManager.RENDER_PRIORITY);
		} catch (IOException e) {
		}
	}

	public Manageable manageableReadResolve ()
	{
		return this;
	}

	public Object manageableWriteReplace ()
	{
		if (hasContent() && getProvider(true) instanceof EmbeddedFileObject) {
			((EmbeddedFileObject)getProvider(true)).writeUpdateFile();
		}
		return this;
	}


	public static class GetExtFromMimeType implements ItemCriterion
	{
		private final Registry registry;


		public GetExtFromMimeType (Registry registry)
		{
			this.registry = registry;
		}

		public boolean isFulfilled (Item item, Object info)
		{
			return (item instanceof ExtensionItem)
					&& ((ExtensionItem) item).getMimeType().
					equals((MimeType)info);
		}


		public String getRootDirectory ()
		{
			return "/io/filetypes";
		}
	}
}
