<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">

<book id="groimp-manual" xmlns:xlink="http://www.w3.org/1999/xlink" >
	<title>
		
		GroIMP User Manual
		
		

	</title>
	<bookinfo>
		<title>
		
		GroIMP User Manual
		
				</title>
		<pubdate></pubdate>
		<copyright>
			<year>2006</year>
		<holder>Lehrstuhl Grafische Systeme,
Brandenburgische Technische Universität Cottbus</holder>
		</copyright>
		
	</bookinfo>
  <chapter xml:id="preface">
    <title>Preface</title>
    <para>
      Dear user of GroIMP,
    </para>
    <para>
      this is the user manual of GroIMP, the GROGRA-related Interactive
      Modelling Platform. GroIMP is OSI certified open source software
      and available at the web page
      <link xlink:href="http://www.grogra.de">www.grogra.de</link>.
      Currently, the GroIMP software is in a BETA state, which means
      that malfunctions may occur, though the software has been tested
      in-depth. The latest news, help and contact to the community can
      be found on the <link xlink:href="http://www.grogra.de">web
      page</link>, the
      <link xlink:href="http://http://wiki.grogra.de/doku.php?id=start">wiki</link>,
      the <link xlink:href="https://gitlab.com/grogra/groimp">code
      repository</link> and our
      <link xlink:href="https://groimp.slack.com/">slack server</link>.
    </para>
    <para>
      GroIMP arose out of academic research in the field of plant
      modelling, mainly carried out by the
      <link xlink:href="http://www-gs.informatik.tu-cottbus.de/">Chair
      for Practical Computer Science / Graphics Systems</link> at the
      Brandenburg University of Technology Cottbus (Germany) and the
      <link xlink:href="http://fbi.forst.uni-goettingen.de/">Plant
      Modelling Group</link> at the Institute of Forest Biometry and
      Informatics at the University of Gottingen (Germany). During
      research, techniques of rule-based plant-modelling were developed
      and refined, and quickly it became clear that these rather
      abstract techniques have to be accompanied by a smart, interactive
      and user-friendly software platform with an up-to-date graphical
      user interface. The result of the effort of bringing together
      elaborated rule-based modelling and user-friendly, interactive
      software is GroIMP and its various plug-ins, which extend GroIMP
      with additional functionality.
    </para>
    <para>
      In the current version, GroIMP supports 3D modelling with
      OpenGL-based rendering, including various forms of NURBS surfaces,
      rule-based modelling using Lindenmayer systems and rule-based
      modelling using Relational Growth Grammars (RGG) expressed in the
      programming language XL. It also contains a 2D interface for
      visualization and modification of network-like structures.
    </para>
    <para>
      As GroIMP is open source software, you are invited to contribute
      to this software. Have a look at the web page to get the latest
      news and source code!
    </para>
    <para>
      In its current state, this user manual is by far not complete. It
      explains some basics of how to work with GroIMP’s user interface,
      more detailed documentation will be published later.
    </para>
    <para>
      We wish you much fun and success with GroIMP.
    </para>
    <para>
      The GroIMP development team
    </para>
  </chapter>
  <chapter xml:id="installation">
    <title>Installation</title>
    <para>
      GroIMP is written in Java. Thus, it should follow Java’s
      WORA-principle (Write Once, Run Anywhere) and should run on every
      Java-capable platform. GroIMP has been tested successfully on Unix
      and Windows platforms.
    </para>
    <para>
      GroIMP 2.1.6 requires the installation of a Java runtime
      environment of version 17. Which can be obtained from Oracle at
      [[https://www.oracle.com/|www.oracle.com]].
    </para>
    <para>
      For a reasonably performant runtime behaviour, at least 256 MB RAM
      and a 1 GHz processor are recommended.
    </para>
    <section xml:id="download">
      <title>Download</title>
      <para>
        The binary distribution of GroIMP is available on the
        [[https://gitlab.com/grogra/groimp/-/releases|gitlab release
        section]] as a zip-file, windows exe, or debian release.
      </para>
    </section>
    <section xml:id="installation-installation-from-binaries">
      <title>Installation Installation from binaries</title>
      <para>
        The windows exe and debian release are executable files that
        will extract and install GroIMP on your computer. It will also
        provide you to create a menu entry and a shortcut. Mac users
        need to download the zip-file, extract it and run GroIMP from
        command line.
      </para>
      <para>
        The directory structure of the zip-file has to be preserved,
        check that your zip program is set up accordingly (and does not
        extract all files into one and the same directory).
      </para>
      <para>
        After extraction of the zip-file, the following files, among
        others, are present in the installation directory:
      </para>
      <para>
        <literal>README</literal>
      </para>
      <para>
        This plain text file contains additional information about the
        GroIMP distribution. Read it carefully.
      </para>
      <para>
        <literal>INSTALL</literal>
      </para>
      <para>
        This plain text file contains instructions for the installation
        of the GroIMP distribution, legal notices and other information.
        Read it carefully. If you have troubles with the installation
        procedure described in this manual, consult the file
        <literal>INSTALL</literal> a solution.
      </para>
      <para>
        <literal>plugins</literal>
      </para>
      <para>
        This directory contains all the plugins that are part of the
        binary distribution of GroIMP.
      </para>
      <para>
        <literal>core.jar</literal>
      </para>
      <para>
        This file is an executable java archive and the entry-point for
        the Java runtime environment to start the GroIMP application.
      </para>
    </section>
    <section xml:id="running-groimp">
      <title>Running GroIMP</title>
      <para>
        Because <literal>core.jar</literal> is an executable java
        archive, it is possible to start GroIMP just by
        (double-)clicking on the file in your system’s file browser as
        you would do it with a usual executable file. This requires a
        suitable setup of your file browser, see the documentation of
        the browser and your Java installation if it does not already
        work.
      </para>
      <section xml:id="from-a-shortcut">
        <title>From a Shortcut</title>
        <para>
          You can set up your desktop to show a menu entry for GroIMP.
          How this is done depends on your system. Usually your system
          provides a menu editor where you can create a new menu entry
          and specifiy the command to be executed when the entry is
          activated. Choose the command as you would do on command line.
        </para>
      </section>
      <section xml:id="from-the-command-line">
        <title>From the Command Line</title>
        <para>
          The file <literal>core.jar</literal> in your installation
          directory of GroIMP’s binary distribution is the entry-point
          for starting GroIMP. Running GroIMP from the command line is
          easy:
        </para>
        <blockquote>
          <para>
            <emphasis role="strong">java -jar
            path/to/core.jar</emphasis>
          </para>
        </blockquote>
        <para>
          Of course, <literal>path/to/core.jar</literal> has to be
          replaced by the actual path to the file
          <literal>core.jar</literal>. E.g., if, on a Unix system, the
          installation directory is <literal>/usr/lib/groimp</literal>,
          the command would be
        </para>
        <blockquote>
          <para>
            <emphasis role="strong">java -jar
            /usr/lib/groimp/core.jar</emphasis>
          </para>
        </blockquote>
        <para>
          or, on a Windows system with installation directory
          <literal>C:\Program Files\GroIMP</literal>,
        </para>
        <blockquote>
          <para>
            <emphasis role="strong">java -Xverify:none -jar
            <quote>C:\Program Files\GroIMP\core.jar</quote></emphasis>
          </para>
        </blockquote>
      </section>
    </section>
    <section xml:id="additional-options">
      <title>Additional Options</title>
      <section xml:id="java-parameters">
        <title>Java Parameters</title>
        <para>
          The <emphasis role="strong">java</emphasis> command has
          several options which influence the runtime behaviour, see the
          documentation of your Java installation. These options have to
          be specified before the <literal>-jar</literal> option. E.g.,
          for the Java runtime environment of Sun, the option
          <literal>-Xmx</literal> specifies the maximum amount of heap
          memory size that should be allocated. The default value of
          this option may be much smaller than your installed main
          memory size. In this case GroIMP cannot benefit from your
          memory until you specify the <literal>-Xmx</literal> option as
          in
        </para>
        <blockquote>
          <para>
            <emphasis role="strong">java -Xmx400m -jar
            path/to/core.jar</emphasis>
          </para>
        </blockquote>
        <para>
          which allows a maximum heap memory allocation of 400 MB. If
          you encounter <literal>OutOfMemoryError</literal>s when
          running GroIMP, you have to specify this option with a larger
          value.
        </para>
      </section>
      <section xml:id="groimp-parameters">
        <title>GroIMP Parameters</title>
        <para>
          The command line also enables to provides parameters to
          GroIMP. These parameters need to be passed AFTER the
          <literal>core.jar</literal> file. E.g.
        </para>
        <blockquote>
          <para>
            <emphasis role="strong">java -jar core.jar
            –project-tree</emphasis>
          </para>
        </blockquote>
        <para>
          The complete list of GroIMP parameters can be found
          <link xlink:href="http://wiki.grogra.de/doku.php?id=user-guide:advanced-groimp-arguments">online</link>
        </para>
      </section>
    </section>
  </chapter>
  <chapter xml:id="plugin-architecture">
    <title>Plugin Architecture</title>
    <section xml:id="java-plugins">
      <title>Java plugins</title>
      <para>
        Installation of Plugins for the Java Image I/O Framework.
      </para>
      <para>
        GroIMP requires only a Java runtime environment in order to run.
        However, the capabilities of some parts of the Java runtime
        environment can be extended by the installation of additional
        plugins. GroIMP makes use of the Java Image I/O Framework which
        provides such a pluggable architecture: The user may install
        plugins which add support for an image file format to the Java
        Image I/O Framework. If the standard installation of the Java
        runtime environment does not support an image file format you
        need, please consult the
        <link xlink:href="http://java.sun.com/j2se/1.4.2/docs/guide/imageio/index.html">Java
        Image I/O pages</link> for a suitable plugin.
      </para>
    </section>
    <section xml:id="groimp-plugins">
      <title>GroIMP Plugins</title>
      <para>
        GroIMP is developed around a plugin architecture. New plugin can
        easily be added to your GroIMP installation by simply adding
        them to the loading directory of GroIMP.
      </para>
      <para>
        On startup GroIMP load all plugins under it’s installed
        path/plugins, as well as plugins in the
        <literal>user plugin path</literal>. This user plugin path can
        be modified in the GroIMP options, and is by default at
        ~/.grogra.de-platform/plugins/.
      </para>
      <para>
        GroIMP plugins can be installed/ removed/ modified from within
        GroIMP with the Plugin Manager. The plugin manager can be found
        in GroIMP on
        <link xlink:href="plugin:de.grogra.plugin.manager/doc/index.html">help/plugin-manager</link>.
        More information can be found in its own documentation. A
        complete list of available plugin can be found
        <link xlink:href="https://grogra.gitlab.io/groimp-utils/plugin_web_explorer/">online</link>.
      </para>
    </section>
  </chapter>
  <chapter xml:id="using-groimp">
    <title>Using GroIMP</title>
    <section xml:id="user-interface">
      <title>User Interface</title>
      <para>
        GroIMP is equipped with a modern, configurable graphical user
        interface. It supports user-defined layouts of panels, including
        arrangement of panels in tab groups and floating windows.
        Screenshot of GroIMP shows a typical screenshot with 3D view.
      </para>
      <figure>
        <title>Screenshot of GroIMP</title>
        <mediaobject>
          <imageobject>
            <imagedata fileref="res/GroIMP.jpg" />
          </imageobject>
          <textobject><phrase>Screenshot of GroIMP</phrase></textobject>
        </mediaobject>
      </figure>
      <para>
        You can see the panels View, Attribute Editor, File Explorer,
        Text Editor, Toolbar and Statusbar. The panels Images and
        Materials are hidden, they are arranged together with File
        Explorer in a <emphasis>tab group</emphasis>. The panel Text
        Editor is not contained in GroIMP's main window but in its own
        <emphasis>floating window</emphasis>. Details of these panels
        will be described in the following.
      </para>
    </section>
    <section xml:id="common-panel-handling">
      <title>Common Panel Handling</title>
      <para>
        GroIMP's user interface consists of an arrangement of panels,
        e.g., a 3D view or a text editor. This arrangement can be
        changed by the user by Drag&amp;Drop: For panels with title bars
        or panels in a tab group, a Drag&amp;Drop operation is performed
        by dragging title bars with the mouse. A contour indicates where
        the panel will be dropped if you release the mouse button. For
        panels without title bars (e.g., toolbars, statusbars), just
        drag the panels themselves.
      </para>
      <para>
        Every panel has a panel menu which provides at least the
        operations Undock and Close. A panel menu is opened by clicking
        on the title bar of the panel or, if there is no title bar, on
        the panel itself (usually with the right mouse button). The
        Undock operation undocks the panel out of its current location
        and brings it up in a new floating window, the Close operation
        closes a panel.
      </para>
      <para>
        Normally, not all possible panels are actually present in the
        user interface. However, all panels are accessible through the
        Panels menu of GroIMP's main window. If a panel you need is
        missing, just select it in the main Panels menu. This brings up
        the panel in a new floating window.
      </para>
      <para>
        The arrangement of all panels is called a panel
        <emphasis>layout</emphasis>. The menu Panels/Set Layout provides
        a set of predefined layouts which are useful for distinct tasks;
        the menu item Panels/Add Layout adds the current layout to the
        list of user-defined layouts of the current project.
        User-defined layouts are also accessible in the menu Panels/Set
        Layout.
      </para>
    </section>
    <section xml:id="groimps-projects">
      <title>GroIMP's Projects</title>
      <para>
        The main entity you work on in GroIMP is the
        <emphasis>project</emphasis>. A project may consist of various
        parts, e.g., files, source code, a scene (2D, 3D, or other),
        resource objects like data sets, 3D materials or the like.
        Several projects can be open at the same time, each in its own
        main window.
      </para>
      <para>
        You create a new project by choosing the menu item
        File/New/Project. Via File/Open, projects can be read from a
        file. Different file formats are available: GS and GSZ are
        GroIMP's native project formats, the other formats are imported
        into a new project.
      </para>
      <para>
        Saving of a project is done in the File menu too. Here, only GS
        and GSZ are available as file formats. The GS file format in
        fact consists of a set of files written in the folder containing
        the GS file: <literal>graph.xml</literal> contains the scene
        graph of the project, the folder <literal>META-INF</literal>
        some meta information about the files, and if there are files
        included in the project, they will be written in the folder (or
        subfolders) too. To avoid conflicts between different projects,
        it is mandatory to use an own folder for each project.
      </para>
      <para>
        Contrary to the GS file format, the GSZ file format only
        consists of a single file. This file is actually a file in the
        common zip-format with special content: It contains all the
        files which comprise a project in the GS file format in a single
        zip archive. You can use standard zip tools to examine or even
        modify its contents.
      </para>
      <para>
        While working with an open project, there is one difference
        between projects in GS/GSZ file format: If you modify and save
        files (e.g., text files) contained in a GS project, they will be
        written to your file system immediately because the GS file
        format consists of a set of files in your file system. However,
        for a GSZ project, these files are written to an internal
        storage: Your modifications are persistently saved only when the
        whole project is saved.
      </para>
    </section>
    <section xml:id="common-panels">
      <title>Common Panels</title>
      <section xml:id="file-explorer">
        <title>File Explorer</title>
        <para>
          GroIMP's File Explorer shows all those files of the project
          which have an immediate meaning as files to GroIMP. These are
          source code files, plain text files, HTML files and the like.
          Files which are used to define non-file-like objects (e.g.,
          images, 3D geometry, data sets) are not shown in the file
          explorer panel, they are accessible in the panels of the
          corresponding object type.
        </para>
        <figure>
          <title>File Explorer</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="res/fileexplorer.png" />
            </imageobject>
            <textobject><phrase>File Explorer</phrase></textobject>
          </mediaobject>
        </figure>
        <para>
          If the file explorer is not already shown, open the panel via
          the Panel menu. As you will know it from your system's file
          browser, GroIMP's file exporer displays files in a tree-like
          structure. Files may be activated by a double-click or
          pressing the Enter key: On activation, source code and text
          files are opened in the text editor, HTML files are shown in
          GroIMP's integrated browser. At the moment, files cannot be
          renamed, moved or deleted in the file explorer.
        </para>
        <para>
          Files which already exist in your file system can be added to
          a project via the menu item Object/New/Add File. You have the
          choice to <emphasis>link</emphasis> or
          <emphasis>add</emphasis> these files: A linked file does not
          become part of the project, the project just references it in
          your file systems. Changes to a linked file take effect on the
          project when GroIMP (re-)opens the file. In contrast to a
          linked file, an added file is copied into the project, thus,
          after addition, there is no connection between the original
          file and the project any more.
        </para>
        <para>
          In File Explorer, a file explorer panel is shown.
          <literal>FTree.rgg</literal> is a source code file,
          <literal>FTree.txt</literal> a text file which contains some
          explanations about the project FTree, and
          <literal>index.html</literal> is a link (indicated by the
          small arrow at the lower left corner of the icon) to an HTML
          file.
        </para>
      </section>
      <section xml:id="text-editor">
        <title>Text Editor</title>
        <para>
          GroIMP is equipped with a simple internal text editor. When
          you activate a source code or text file entry in the file
          explorer, it is opened in the text editor and can be edited.
          The usual editing operations (Cut&amp;Paste, Undo/Redo, Save)
          are available. Text Editor f-texteditor shows a screenshot.
        </para>
        <figure>
          <title>Text Editor</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="res/texteditor.png" />
            </imageobject>
            <textobject><phrase>Text Editor</phrase></textobject>
          </mediaobject>
        </figure>
        <para>
          For some file types, additional actions are triggered when a
          file of that type is saved. For example, source code is
          compiled immediately.
        </para>
      </section>
      <section xml:id="text-editor-jedit">
        <title>Text Editor jEdit</title>
        <para>
          If the jEdit-Plugin is installed, the
          <link xlink:href="http://www.jedit.org/">jEdit</link> text
          editor is used instead of GroIMP's simple internal text
          editor. Text Editor jEdit shows a screenshot. jEdit supports
          syntax highlighting and contains various edit, search, and
          print commands. Comprehensive documentation is available via
          the Help-menu of jEdit.
        </para>
        <figure>
          <title>Jedit texteditor</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="res/texteditor_jedit.png" />
            </imageobject>
            <textobject><phrase>Jedit texteditor</phrase></textobject>
          </mediaobject>
        </figure>
      </section>
      <section xml:id="image-explorer">
        <title>Image Explorer</title>
        <para>
          Every image used within a GroIMP project, e.g., a colouring
          texture of a 3D material, is shown in the Image Explorer. Its
          structure is similar to the File Explorer. New images can be
          added to a project via the menu Object/New: Currently, this
          menu contains only the item From File which reads an existing
          image file into the project. All file formats which are
          supported by your installation of the Java Image I/O Framework
          are readable, these are at least Portable Network Graphics
          (<literal>png</literal>), JPEG and Graphics Interchange Format
          (<literal>gif</literal>). If a format you need is not
          supported by your installation, see
          <link linkend="s-imageio">Installation of Plugins for the Java
          Image I/O Framework</link>. As for the File Explorer, you have
          the choice to link or add image files to the project.
        </para>
        <figure>
          <title>Image Explorer, Material Explorer, Attribute
          Editor</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="res/imgmatattr.png" />
            </imageobject>
            <textobject><phrase>Image Explorer, Material Explorer,
            Attribute Editor</phrase></textobject>
          </mediaobject>
        </figure>
      </section>
      <section xml:id="attribute-editor">
        <title>Attribute Editor</title>
        <para>
          Another panel which you will encounter often is the attribute
          editor. It is used to edit attributes of a variety of objects:
          User settings of configuration objects, geometric attributes
          of scene objects, attributes of resource objects like 3D
          materials, and others.
        </para>
        <para>
          Each attribute has a type, and each type brings up specific
          edit components in the attribute editor. For example, the
          radius attribute of circles or spheres is a numeric attribute
          and is editable through a simple input field. The orientation
          of a cylinder in 3D space is a 3D vector and, thus, it is
          editable through a set of three numeric input fields. Very
          complex attributes like 3D materials consist of several
          subattributes, each of which brings up its own edit components
          in the attribute editor.
        </para>
        <para>
          In Image Explorer, Material Explorer, Attribute Editor, you
          can see the attribute editor for a 3D material of type Phong.
          Such a material has the attributes Channel Input, Diffuse
          Colour, Transparency, Interpolated Transparency, Ambient
          Colour, Emissive Colour, Specular Colour and Shininess. The
          Phong material shown has a Checkerboard 2D surface map as its
          Diffuse Colour, the checkerboard itself has the attributes
          Channel Input, Colour 1 and Colour 2. As Channel Input, an
          UV-Transformation is chosen, its numeric attributes are
          editable through the input fields. For angles and other
          attributes which represent physical quantities, their units
          are shown, in the this case <literal>deg</literal> which
          stands for degrees. As Colour 1, an RGB colour is chosen,
          whose values are modified by sliders.
        </para>
        <para>
          Colour 2 of the checkerboard is set to an image map. One could
          have chosen an RGB colour as for the first colour, a
          Checkerboard 2D as for the diffuse colour of the material, a
          simple constant colour, or some other valid type for this
          attribute. In GroIMP, there are many attributes whose values
          may be of a set of different types. For all these attributes,
          a button is shown in the attribute editor which pops up a menu
          to choose the desired type. Once a type has been chosen, the
          attribute value is set to this type, and appropriate edit
          components are created. They are shown within a minimizable
          subpanel. To minimize it (and to maximize it later on), click
          on its upper border where a small button is displayed. For
          example, the subpanel for the RGB components of the Opacity
          attribute is minimized in Image Explorer, Material Explorer,
          Attribute Editor. A double-click on the upper border maximizes
          the subpanel and minimizes all other panels.
        </para>
      </section>
      <section xml:id="preferences">
        <title>Preferences</title>
        <para>
          In GroIMP the preferences (from java preferences) are the
          software options. Most GroIMP options are managed by the
          Preference panel, which can be open from the menu at
          Panel&gt;Preferences. By default, options are managed
          globally. i.e. when an option is set, every workbench will use
          it. There is an exception though. It is possible to setup an
          option file for a workbench, thus, using options defined in
          that file instead of the global ones.
        </para>
        <figure>
          <title>Preferences Panel (Metal Look &amp; Feel)</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="res/preferences.png" />
            </imageobject>
            <textobject><phrase>Preferences Panel (Metal Look &amp;
            Feel)</phrase></textobject>
          </mediaobject>
        </figure>
        <section xml:id="global-options">
          <title>Global options</title>
          <para>
            Usually when a user change the options of GroIMP, he changes
            the global options. These options are stored when set in the
            java preferences (in
            <literal>~/.java/.userPrefs/de/grogra/options/</literal> +
            path of the option). This enables the options to be
            persistent between GroIMP uses. It also makes the options
            persistent when an other version of GroIMP is installed.
          </para>
          <para>
            Global options are used by every workbenches that do not
            include an option file.
          </para>
          <para>
            The panel of global options can be open if the currently
            used workbench do not have an option file. In that case,
            when the preference panel is open (with
            <literal>Panel&gt;Preferences</literal>) you can see the
            Label on the bottom right that tells the current panel is
            Global (see the red circle on the Image below).
          </para>
          <figure>
            <title>Global preference panel</title>
            <mediaobject>
              <imageobject>
                <imagedata fileref="res/global_preferences_panel.png" />
              </imageobject>
              <textobject><phrase>Global preference
              panel</phrase></textobject>
            </mediaobject>
          </figure>
        </section>
        <section xml:id="workbench-options">
          <title>Workbench options</title>
          <para>
            GroIMP also support options specific for a given workbench
            (since version 2.1.4). The option file (called
            <quote>workbench.options</quote>) is shipped with the GroIMP
            project (at the root of the project.gs file, or in the .gsz
            archive). It enables to share a project with a specific set
            of options.
          </para>
          <para>
            When the option file exists in a project, GroIMP will use
            its options instead of the global ones. If an option file
            contains more options than the current instance of GroIMP,
            these options are kept but not use. If the option miss some
            options that the current instance of GroIMP defined, these
            options are added to the option file when saved.
          </para>
          <para>
            The option file can be added in a workbench from the
            preference panel (Panel&gt;Preferences). On the menu bar
            <literal>file&gt;options&gt;Create current workbench option file</literal>.
          </para>
          <figure>
            <title>menu options</title>
            <mediaobject>
              <imageobject>
                <imagedata fileref="res/panel_opt_add.png" />
              </imageobject>
              <textobject><phrase>menu options</phrase></textobject>
            </mediaobject>
          </figure>
          <para>
            Once the option file is added, the view of the preference
            panel will change into the workbench option view In that
            view the name of the workbench whose options are managed is
            displayed in the red circle 1. On the bottom right a button
            to open the global options is available (see the red circle
            2).
          </para>
          <figure>
            <title>workbench option panel</title>
            <mediaobject>
              <imageobject>
                <imagedata fileref="res/wb_preferences_panel.png" />
              </imageobject>
              <textobject><phrase>workbench option
              panel</phrase></textobject>
            </mediaobject>
          </figure>
          <para>
            When a option file is created and added to the project it is
            automatically setup with all the existing current global
            options. When modifying workbench options, the options new
            value is directly updated, however the option file is not
            automatically updated. To push the modification to the
            option file the project needs to be saved.
          </para>
          <para>
            To stop using the option file of a workbench, it can be
            deleted from the menu option:
            <literal>file&gt;options&gt;Delete</literal> current option
            file.
          </para>
          <para>
            The file can be modified outside of GroIMP as plain text. To
            add an option file in a project outside of GroIMP, the file
            needs to be added <emphasis>and</emphasis> the registry item
            OptionsSource needs to be added to the project.gs. Same when
            deleting the option file from outside of GroiMP: both the
            both the file and the registry item needs to be deleted. The
            MANIFEST.MF file must be updated as well.
          </para>
        </section>
        <section xml:id="options-for-the-swing-user-interface">
          <title>Options for the Swing User Interface</title>
          <para>
            The group UI Toolkit/Swing UI in the Preferences panel lets
            you choose the Look &amp; Feel of the Swing user interface
            and whether window decorations (title bar, borders, etc.)
            should be provided by Swing or by the native window manager
            of your system. The set of available Look &amp; Feels
            depends on your Java and GroIMP installations: GroIMP's
            binary distribution is bundled with the Kunststoff, Liquid
            and Tonic Look &amp; Feels.
          </para>
          <para>
            If you switch to another Look &amp; Feel in the Preferences
            panel, the user interface will be redrawn immediately.
            However, changing the window decorations option only takes
            effect when a new window is opened. Also, there are Look
            &amp; Feels which do not support window decorations.
          </para>
        </section>
      </section>
      <section xml:id="other-explorer-panels">
        <title>Other Explorer Panels</title>
        <para>
          Besides files and images, a variety of other object kinds is
          displayed within an explorer panel. For example, Image
          Explorer, Material Explorer, Attribute Editor shows the
          Material Explorer panel. These panels are always similar to
          the file explorer or the image explorer: Objects are shown in
          a hierarchical tree-like layout, and new objects are created
          by the Object/New menu of the panel. The contents in this menu
          depend on the explorer: The Image Explorer reads in images
          from files, the Material Explorer allows the creation of new
          materials of several material types, etc.
        </para>
      </section>
    </section>
    <section xml:id="import-and-export-formats-mtg---multiscale-tree-graph">
      <title>Import and Export Formats: MTG - Multiscale Tree
      Graph</title>
      <para>
        EXAMPLE - Importing and Using MTG data in GroIMP
      </para>
      <para>
        ------------------------------------------------------------------------
      </para>
      <para>
        **NOTE: In this example, a .mtg file is already imported and a
        .rgg file has already been added into the project.
      </para>
      <para>
        Begin at step 1 if you wish to find out how to load a .mtg file
        into GroIMP.
      </para>
      <para>
        Skip to step 6 if you wish to know how to modify the loaded MTG
        data in this example project.
      </para>
      <itemizedlist spacing="compact">
        <listitem>
          <para>
            1. Open an MTG file
          </para>
        </listitem>
        <listitem>
          <para>
            2. MTG node classes and corresponding GroIMP modules
          </para>
        </listitem>
        <listitem>
          <para>
            3. MTG feature attributes and GroIMP module variables
          </para>
        </listitem>
        <listitem>
          <para>
            4. Visualizing the MTG data
          </para>
        </listitem>
        <listitem>
          <para>
            5. Saving the MTG graph in a GroIMP project
          </para>
        </listitem>
        <listitem>
          <para>
            6. Modifying the loaded MTG graph
          </para>
        </listitem>
      </itemizedlist>
    </section>
  </chapter>
</book>