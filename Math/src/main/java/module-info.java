module math {
	exports de.lmu.ifi.dbs.elki.math.geometry;
	exports de.grogra.math.weiszfeld;
	exports de.lmu.ifi.dbs.elki.utilities.datastructures.iterator;
	exports de.grogra.math.util;
	exports de.lmu.ifi.dbs.elki.data;
	exports de.lmu.ifi.dbs.elki.utilities.pairs;
	exports de.grogra.math.convexhull;
	exports de.lmu.ifi.dbs.elki.utilities;
	exports de.lmu.ifi.dbs.elki.utilities.datastructures.arraylike;
	exports de.grogra.math;
	exports de.lmu.ifi.dbs.elki;
	exports de.lmu.ifi.dbs.elki.math;
	exports de.lmu.ifi.dbs.elki.math.linearalgebra;
	exports de.lmu.ifi.dbs.elki.utilities.optionhandling;
	exports de.lmu.ifi.dbs.elki.data.spatial;
	exports de.grogra.pf.math;
	exports de.lmu.ifi.dbs.elki.persistent;

	requires graph;
	requires platform;
	requires platform.core;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.desktop;
}