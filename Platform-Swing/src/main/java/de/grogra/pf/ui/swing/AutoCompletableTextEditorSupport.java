package de.grogra.pf.ui.swing;

import java.lang.reflect.InvocationTargetException;

import de.grogra.persistence.Transaction;
import de.grogra.persistence.XAListener;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Option;
import de.grogra.pf.ui.TextEditor;
import de.grogra.pf.ui.autocomplete.AutoCompletable;
import de.grogra.pf.ui.autocomplete.AutoCompletorFactory;
import de.grogra.pf.ui.autocomplete.AutoCompletors;
import de.grogra.pf.ui.autocomplete.impl.AbstractAutoCompletor;
import de.grogra.util.Map;

public abstract class AutoCompletableTextEditorSupport extends PanelSupport 
	implements TextEditor, AutoCompletable, XAListener {

	/**
	 * a registry item that is the directory of the autocompletor options. When
	 * the autocompletor option change it trigger an xaevent. That item is used 
	 * to catch that event.
	 */
	Item ac;
	/**
	 * The factory that can produce the autocompletor object. 
	 */
	AutoCompletorFactory currentAcFact;
	AbstractAutoCompletor autoCompl;

	public AutoCompletableTextEditorSupport ()
	{
		super (new SwingPanel (null));
	}

	public AutoCompletableTextEditorSupport(SwingPanel swingPanel) {
		super (swingPanel);
	}

	@Override
	protected void configure (Map params)
	{
		super.configure(params);
		autoCompl=null;
		ac = Item.resolveItem(getWorkbench(), "/ui/autocompletors/autocomp");
		if (ac!=null) {
			ac.getPersistenceManager().addXAListener(this);
			Item acs= ac.getItem("acs");
			if (acs!=null && acs instanceof Option) {
				updateAutoCompletor();
			}
		}
	}

	@Override
	protected void disposeImpl ()
	{
		if (ac!=null) {
			uninstallAutoCompletor();
			ac.getPersistenceManager().removeXAListener(this);
		}
		super.disposeImpl();
	}

	@Override
	public void transactionApplied (Transaction.Data xa, boolean rollback)
	{
		updateAutoCompletor ();
	}


	private void updateAutoCompletor ()
	{
//		final AutoCompletorFactory fac = AutoCompletors.getCompletorFactory( (String) ((Option)ac).getObject());
//		try {
//			if ( (fac != null) && (!fac.equals (currentAcFact) || autoCompl == null))
//			{
//				currentAcFact = fac;
//				uninstallAutoCompletor();
//				installAutoCompletor(autoCompl=fac.getAutoCompletor());
//			}
//		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException
//				| InvocationTargetException | NoSuchMethodException e) {
//			e.printStackTrace();
//		}
		de.grogra.graph.impl.Node opt = ac.getBranchTail ();
		while (opt != null) {
			if (opt instanceof Option) {
				if (opt.getName().contentEquals("acs")) {
					final AutoCompletorFactory fac = 
							AutoCompletors.getCompletorFactory( (String) ((Option)opt).getObject());
					try {
						if ( (fac != null) && (!fac.equals (currentAcFact) || autoCompl == null))
						{
							currentAcFact = fac;
							uninstallAutoCompletor();
							installAutoCompletor(autoCompl=fac.getAutoCompletor());
						}
					} catch (ClassNotFoundException | IllegalAccessException | InstantiationException
							| InvocationTargetException | NoSuchMethodException e) {
						e.printStackTrace();
					}
				}
				if (opt.getName().contentEquals("showDescriptionPopup")){
					if (autoCompl != null &&
							(Boolean)((Option)opt).getObject() != autoCompl.getShowDescWindow() ) {
						autoCompl.setShowDescWindow((Boolean)((Option)opt).getObject() );
					}
				}

				if (opt.getName().contentEquals("automaticallyCompleteOne")){
					if (autoCompl != null &&
							(Boolean)((Option)opt).getObject() != autoCompl.getAutoCompleteSingleChoices() ) {
						autoCompl.setAutoCompleteSingleChoices((Boolean)((Option)opt).getObject());
					}
				}
			}

			opt = opt.getPredecessor();
		}

	}
}
