package de.grogra.pf.ui.swing.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.TreePath;

import de.grogra.pf.datatransfer.UIDataHandler;
import de.grogra.pf.datatransfer.UITransferHandler;
import de.grogra.pf.datatransfer.UITransferable;
import de.grogra.pf.datatransfer.UITransferableBase;
import de.grogra.pf.registry.ObjectItem;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.pf.ui.swing.SwingTree;
import de.grogra.pf.ui.tree.UINodeHandler;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.pf.ui.tree.UITreePipeline;
import de.grogra.util.MappedTree;
import de.grogra.util.MutableTree;

/**
 * TransferHandler that create data in/out of SwingTree.
 */
public class TreeTransferHandler extends TransferHandler implements UITransferHandler {
	private static final long serialVersionUID = -6817955101045751121L;
	List<UIDataHandler> handlers;

	public TreeTransferHandler() {
	}

	@Override
	public void setDataHandlers(List<UIDataHandler> d) {
		this.handlers = d;
	}

	@Override
	public DataFlavor[] getDataFlavors() {
		return handlers.stream().map(c -> c.getDataFlavors()).toArray(DataFlavor[]::new);
	}

	@Override
	public UITransferable createTransferable(Object source) {
		if (!(source instanceof SwingTree)) { return null; }
		SwingTree tree = (SwingTree)source;
		TreePath[] paths = tree.getSelectionPaths();
		if (paths == null) {
			return null;
		}
		Object x = paths[0].getLastPathComponent();
		Object ob= tree.getNode(x);
		UITree ut = tree.getUITree();
		String n = ut.getName(ob);
		MappedTree.Node firstNode =(MappedTree.Node)x; 
		Object o = firstNode.getSourceNode();
		if (o instanceof UITreePipeline.Node) { // case of tree in tree (search explorer)
			o = ((UITreePipeline.Node)o).getNode();
		}
		if (o instanceof ObjectItem) { // case of reference objects (e.g. images)
			o = ((ObjectItem)o).getObject();
		} 

 		return new UITransferableBase(source, o,n);
	}

	@Override
	public List<UIDataHandler> getHandlers(){
		return handlers;
	}

	public boolean canImport(TransferHandler.TransferSupport support) {
		if (handlers == null) { return false; }
		if(!support.isDrop()) {
			return false;
		}
		support.setShowDropLocation(true);

		boolean canImport = false;
		for (UIDataHandler h : handlers) {
			if (h.isDataFlavorSupported(support.getDataFlavors())) {
				canImport = true;
				break;
			}
		}
		if(!canImport) {
			return false;
		}

		// Do not allow to drop in another swingtree
//		try {
//			if ( support.getTransferable() instanceof Transferable && 
//					((Transferable)support.getTransferable())
//							.getTransferData(UITransferable.SOURCE) == support.getComponent() ) {
//				return false;
//			}
//		} catch (UnsupportedFlavorException | IOException e) {
//			return false;
//		}

		// Do not allow a drop on the drag source selections.
		JTree.DropLocation dl =
				(JTree.DropLocation)support.getDropLocation();
		JTree tree = (JTree)support.getComponent();
		int dropRow = tree.getRowForPath(dl.getPath());
		int[] selRows = tree.getSelectionRows();
		for(int i = 0; i < selRows.length; i++) {
			if(selRows[i] == dropRow) {
				return false;
			}
			MappedTree.Node treeNode =
					(MappedTree.Node)tree.getPathForRow(selRows[i]).getLastPathComponent();
			if (tree.getRowForPath(new TreePath(((MappedTree.Node)treeNode).getPath())) == dropRow) {
				return false;
			}  
		}

		return true;
	}

	/* 
	 * Map Swing method to call internal createtransferable
	 */
	protected Transferable createTransferable(JComponent c) {
		return createTransferable((Object)c);
	}

	protected void exportDone(JComponent source, Transferable data, int action) {
		return;
	}

	public int getSourceActions(JComponent c) {
		return COPY_OR_MOVE;
	}
	
	public boolean importData(TransferHandler.TransferSupport support) {
		if(!canImport(support)) {
			return false;
		}
		if (!(support.getTransferable() instanceof Transferable)) {
			return false;
		}
		Object node = loadData((Transferable) support.getTransferable());
				
		// Extract transfer data.
		SwingTree tree = (SwingTree)support.getComponent();
		Context c = tree.getUITree ().getContext ();
		JTree.DropLocation dl =
				(JTree.DropLocation)support.getDropLocation();
		int childIndex = dl.getChildIndex();
		TreePath dest = dl.getPath();
		MappedTree.Node parent =
				(MappedTree.Node)dest.getLastPathComponent();
		UI.getJobManager (c).runLater
		(tree, new ActionEditEvent (UINodeHandler.ACTION_MOVE, parent.getSourceNode(), 0)
				.set (c, node), c, JobManager.ACTION_FLAGS);
		return true;
	}

	public String toString() {
		return getClass().getName();
	}
}