module imp3d {
	exports de.grogra.imp3d.ray;
	exports de.grogra.imp3d;
	exports de.grogra.imp3d.io;
	exports de.grogra.imp3d.objects;
	exports de.grogra.imp3d.ray2;
	exports de.grogra.imp3d.math.delaunay;
	exports de.grogra.imp3d.shading;
	exports de.grogra.imp3d.anaglyph;
	exports de.grogra.imp3d.msml;
	exports de.grogra.imp3d.edit;

	requires graph;
	requires imp;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl;
	requires xl.core;
	requires xl.impl;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires java.xml;
	
	opens de.grogra.imp3d to utilities;
	opens de.grogra.imp3d.shading to utilities;
	
	provides javax.imageio.spi.ImageReaderSpi with de.grogra.imp3d.io.HGTImageReaderSpi;
}