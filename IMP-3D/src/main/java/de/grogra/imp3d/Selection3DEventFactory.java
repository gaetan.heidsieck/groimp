package de.grogra.imp3d;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.imp.viewhandler.SelectionEvent;
import de.grogra.imp.viewhandler.SelectionEventFactory;
import de.grogra.imp.viewhandler.SelectionEventImpl;
import de.grogra.imp.viewhandler.ViewEventHandler;
import de.grogra.persistence.SCOType;

/** 
 * It defines the activation events that trigger the selection mode.
 * The default activation events are: 
 * <ul>
 *   <li>Left mouse button pressed over an object in the scene.</li>
 *   <li>Left mouse button dragged with the Left shift pressed.</li>
 * </ul>
*/
public class Selection3DEventFactory extends SelectionEventFactory {

	//enh:sco SCOType
	
	@Override
	public SelectionEvent createEvent
	(ViewEventHandler h, EventObject e) {
		return new SelectionEvent3D(h,e);
	}
	
	public boolean isActivationEvent (ViewEventHandler h, EventObject e)
	{
		if (!(e instanceof MouseEvent))
		{
			return false;
		}
		MouseEvent me = (MouseEvent) e;
		return ((!me.isAltDown() && me.getID () ==MouseEvent.MOUSE_RELEASED) ) ||
				(me.isShiftDown() && SwingUtilities.isLeftMouseButton(me) 
						&& (me.getID () == MouseEvent.MOUSE_DRAGGED));
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (Selection3DEventFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new Selection3DEventFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (Selection3DEventFactory.class);
		$TYPE.validate ();
	}

//enh:end
}
