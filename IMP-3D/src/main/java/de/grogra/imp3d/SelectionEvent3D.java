package de.grogra.imp3d;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import de.grogra.imp.edit.ViewSelection;
import de.grogra.imp.viewhandler.SelectionEventImpl;
import de.grogra.imp.viewhandler.ViewEventHandler;
import de.grogra.imp3d.overlay.Rectangle;

import de.grogra.graph.Path;

public class SelectionEvent3D extends SelectionEventImpl {

	protected int lastX, lastY;
	protected int originX, originY;
	protected boolean dragging=false;

	
	public SelectionEvent3D(ViewEventHandler h, EventObject e) {
		super(h, e);
		lastX = ((MouseEvent) e).getX ();
		lastY = ((MouseEvent) e).getY ();
		originX = lastX;
		originY = lastY;
	}

	@Override
	public void eventOccured(EventObject e) {
		if (e instanceof MouseEvent)
		{
			MouseEvent me = (MouseEvent) e;
			me.consume ();
			switch (me.getID ())
			{
				case MouseEvent.MOUSE_CLICKED:
					selectImpl(me);
				case MouseEvent.MOUSE_DRAGGED:
					dragging=true;
					int dx = me.getX () - lastX, dy = me.getY () - lastY;
					if ((dx == 0) && (dy == 0))
					{
						return;
					}
					lastX += dx;
					lastY += dy;
					// update a rectangle shown on the view
					// pick should be false if highlight on move is disabled
//					handler.getView().createDrawnDnD(originX, originY, lastX, lastY, false);
					handler.getView().getOverlay().clear();
					handler.getView().repaintOverlay(false);
					handler.getView().getOverlay().add(new Rectangle(originX, originY, lastX, lastY));
					handler.getView().repaintOverlay(false);
					break;
				case MouseEvent.MOUSE_RELEASED:
					if (!dragging) { return; }
					if ((lastX != originX) || (lastY != originY)) {
//						handler.getView().createDrawnDnD(originX, originY, lastX, lastY, true);
						select(e);
						handler.getView().getOverlay().clear();
						handler.getView().repaintOverlay(true);
					}
				case MouseEvent.MOUSE_MOVED:
				case MouseEvent.MOUSE_EXITED:
					handler.getView().getOverlay().clear();
					handler.getView().repaintOverlay(true);
					handler.disposeEvent (null);
					dragging = false;
					return;
			}
		}
		if (e instanceof KeyEvent) {
			if (!dragging) {
				return;
			}
			if ( (((KeyEvent)e).getID() == KeyEvent.KEY_RELEASED ) && 
					((KeyEvent)e).getKeyCode() == KeyEvent.VK_SHIFT) {
				if ((lastX != originX) || (lastY != originY)) {
					handler.getView().getNodeInOverlayProjection();
					handler.getView().getOverlay().clear();
					handler.getView().repaintOverlay(true);
				}
				handler.getView().getOverlay().clear();
				handler.getView().repaintOverlay(true);
				handler.disposeEvent (null);
				dragging = false;
				return;
			}
		}
		super.eventOccured(e);
	}
	
	@Override
	protected void selectImpl(MouseEvent e) {
		if (dragging) {
			Path[] paths = handler.getView().getNodeInOverlayProjection();
			ViewSelection s = ViewSelection.get (handler.getView());
			if (s != null)
			{
				if (paths != null && paths.length > 0)
				{
					if (e.isControlDown ())
					{
						for (Path p : paths) {
							s.toggle (ViewSelection.SELECTED, p);
						}
					}
					else
					{
						s.set (ViewSelection.SELECTED | ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER
								, paths, true);				
					}
				}
				else
				{
					s.set (ViewSelection.SELECTED | ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER
							, Path.PATH_0, true);
				}
			}
		} else {
			super.selectImpl(e);
		}
	}

}
