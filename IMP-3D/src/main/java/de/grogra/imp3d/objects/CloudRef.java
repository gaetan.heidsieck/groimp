package de.grogra.imp3d.objects;

import de.grogra.pf.registry.ItemReference;
import de.grogra.pf.registry.Registry;

public class CloudRef extends ItemReference<BoundedCloud>  {

	public CloudRef() {
		super(null);
	}
	public CloudRef(String name) {
		super(name);
	}

	public synchronized BoundedCloud resolve() {
		return objectResolved ? object
				 : resolveObject ("/objects/pointclouds", Registry.current ());
	}
	
//	@Override
//	protected Item createItem (RegistryContext ctx, String dir, String name)
//	{
//		CloudArray cloud = new CloudArray ();
//		ds.setTitle (name);
//		Item i = new SharedValue (name, ds);
//		ctx.getRegistry ().getDirectory (dir, null).add (i);
//		i.makeUserItem (false);
//		return i;
//	}
	
	//enh:sco
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends ItemReference.Type
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CloudRef representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, ItemReference.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CloudRef ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CloudRef.class);
		$TYPE.validate ();
	}

//enh:end

}
