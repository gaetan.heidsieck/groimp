package de.grogra.imp3d.objects;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.imp.PickList;
import de.grogra.imp3d.Pickable;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.Renderable;

/**
 * Simple implementation of a Point Cloud Node. It wrap the Cloud datastructure into
 * an object that is displayble and pickable.
 */
public class PointCloudImpl extends PointCloud implements Renderable, Pickable {

	protected Cloud cloud;
	//enh:field getmethod=getCloud setmethod=setCloud
	
	// size of a point when drawn on screen
	float pointSize = 3.0f;
	// enh:field getter setter

		
	public PointCloudImpl() {
		cloud = new CloudArray();
	}
	
	public PointCloudImpl(Cloud c) {
		setCloud(c);
	}
	
	public PointCloudImpl(float[] f) {
		cloud= new CloudArray(this,f);
	}
	@Override
	public Cloud getCloud() {
		return cloud;
	}

	@Override
	public void setCloud(Cloud c) {
		if (cloud != null) {
			if (cloud.equals(c)) {
				return;
			}
			cloud.dispose(false);
		}
		cloud = c;
		c.setNode(this);
	}

	/* (non-Javadoc)
	 * @see de.grogra.imp3d.Renderable#draw(java.lang.Object, boolean, de.grogra.imp3d.RenderState)
	 */
	public void draw(Object object, boolean asNode, RenderState rs) {
		rs.drawPointCloud(cloud.getCloud().pointsToFloat(), pointSize, color, 
				RenderState.CURRENT_HIGHLIGHT, null);
	}
	
	public void pick(Object object, boolean asNode, Point3d origin,
			Vector3d direction, Matrix4d transformation, PickList list)
	{
		// ray-box intersection from:
		// http://www.cs.utah.edu/~awilliam/box/
		// (Smit's algorithm)
		
		if (!(cloud instanceof BoundedCloud)) {
			return;
		}
		double maxx = ((BoundedCloud)cloud).getMaximumX();
		double minx = ((BoundedCloud)cloud).getMinimumX();
		double maxy = ((BoundedCloud)cloud).getMaximumY();
		double miny = ((BoundedCloud)cloud).getMinimumY();
		double maxz = ((BoundedCloud)cloud).getMaximumZ();
		double minz = ((BoundedCloud)cloud).getMinimumZ();
		
		double tmin, tmax, tymin, tymax, tzmin, tzmax;
		if (direction.x >= 0) {
			tmin = (minx - origin.x) / direction.x;
			tmax = (maxx - origin.x) / direction.x;
		} else {
			tmin = (maxx - origin.x) / direction.x;
			tmax = (minx - origin.x) / direction.x;
		}
		if (direction.y >= 0) {
			tymin = (miny - origin.y) / direction.y;
			tymax = (maxy - origin.y) / direction.y;
		} else {
			tymin = (maxy - origin.y) / direction.y;
			tymax = (miny - origin.y) / direction.y;
		}
		if (tmin > tymax || tymin > tmax)
			return;
		if (tymin > tmin)
			tmin = tymin;
		if (tymax < tmax)
			tmax = tymax;
		if (direction.z >= 0) {
			tzmin = (minz - origin.z) / direction.z;
			tzmax = (maxz - origin.z) / direction.z;
		} else {
			tzmin = (maxz - origin.z) / direction.z;
			tzmax = (minz - origin.z) / direction.z;
		}
		if (tmin > tzmax || tzmin > tmax)
			return;
		if (tzmin > tmin)
			tmin = tzmin;
		if (tzmax < tmax)
			tmax = tzmax;
		
		list.add(tmin);
	}

	
	// enh:insert $TYPE.addIdentityAccessor (Attributes.SHAPE);
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field cloud$FIELD;
	public static final NType.Field pointSize$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (PointCloudImpl.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setFloat (Object o, float value)
		{
			switch (id)
			{
				case 1:
					((PointCloudImpl) o).pointSize = (float) value;
					return;
			}
			super.setFloat (o, value);
		}

		@Override
		public float getFloat (Object o)
		{
			switch (id)
			{
				case 1:
					return ((PointCloudImpl) o).getPointSize ();
			}
			return super.getFloat (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((PointCloudImpl) o).setCloud ((Cloud) value);
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((PointCloudImpl) o).getCloud ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new PointCloudImpl ());
		$TYPE.addManagedField (cloud$FIELD = new _Field ("cloud", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (Cloud.class), null, 0));
		$TYPE.addManagedField (pointSize$FIELD = new _Field ("pointSize", 0 | _Field.SCO, de.grogra.reflect.Type.FLOAT, null, 1));
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new PointCloudImpl ();
	}

	public float getPointSize ()
	{
		return pointSize;
	}

	public void setPointSize (float value)
	{
		this.pointSize = (float) value;
	}

//enh:end
}
