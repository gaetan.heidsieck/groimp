module xl.core {
	exports de.grogra.reflect;
	exports de.grogra.xl.lang;
	exports de.grogra.xl.util;

	requires java.desktop;
}