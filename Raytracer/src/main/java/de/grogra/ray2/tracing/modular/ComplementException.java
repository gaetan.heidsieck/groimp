package de.grogra.ray2.tracing.modular;

public class ComplementException extends Exception {

	public ComplementException() { super();	}
	public ComplementException(String s) {super(s);}
}
