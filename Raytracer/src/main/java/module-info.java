module raytracer {
	exports de.grogra.ray2.light;
	exports de.grogra.ray2.radiosity.triangulation;
	exports org.sunflow.math;
	exports de.grogra.ray.tracing;
	exports de.grogra.ray2.radiosity;
	exports org.sunflow.image;
	exports de.grogra.ray2;
	exports de.grogra.ray.memory;
	exports de.grogra.ray2.metropolis.strategy;
	exports de.grogra.ray2.metropolis;
	exports de.grogra.ray2.tracing;
	exports de.grogra.task;
	exports de.grogra.ray.shader;
	exports de.grogra.ray.physics;
	exports de.grogra.ray.event;
	exports net.goui.util;
	exports de.grogra.ray;
	exports de.grogra.ray.util;
	exports de.grogra.ray2.antialiasing;
	exports de.grogra.ray.antialiasing;
	exports de.grogra.ray.intersection;
	exports de.grogra.ray2.photonmap;
	exports de.grogra.ray.quality;
	exports de.grogra.ray2.tracing.modular;
	exports de.grogra.ray.debug3d;
	exports de.grogra.ray.light;

	requires vecmath;
	requires xl.core;
	requires java.desktop;
	requires kdtree;
	requires utilities;
	
	}