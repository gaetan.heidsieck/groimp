# Plugin Manger for GroIMP

## Description

Groimp is design arround a plugin architecture. The plugin manager aims at making plugins easier to handle.
The plugin manager also contains a GUI accessible from the GroIMP gui.

## Plugin repositories

The plugin manger uses Plugin repositories to gather knowledge on the available plugins. 
The repositories can be provided in three formats:

1) As a JSON file. Which contains the metadata of one or several plugins. The file need to follow the JSON format for plugin data (see bellow).
2) As a local directory. The directory is automatically scanned to find any Groimp plugin. They are found from their _plugin.xml_ file. The plugin data is then extracted from that file.
3) As a list of repositories (_.list_). The _.list_ file is a simple list of any of the three repository formats.


## Add a repository

By default the plugin manager consider two repositories: 
1) The official list of plugin. Which is the plugin_repo.list file from this git repository.
2) The local cache directory if used. Thus, plugins that have been cached in previous executions are known by the plugin manager.

User can add repositories, either through the plugin manager GUI, or through GroIMP's options.

Once added, a repository remain known by GroIMP between different executions. The repository can be activate/deactivate without beeing removed. 

After modifications on the repositories (changing url, adding/deleting, or activating/deactivating), they need to be reloaded. This is performed in the GUI by the menu _data>Reload repositories_. Or by using the command _/pluginmanager/commands/data/reloadRepo_.

## Plugin cache management

The plugin manager support caching data at two level. 

1) The content of the repositories. This enables to load the content of the repositories without requiring fetching them again.
2) The plugin themselves. Once downloaded, or deleted, plugins can be added to the cache. The cached plugins are version dependent. 

The cache can be enabled/disabled in groimp options. 

The cached data can be deleted by the plugin manager. 

The default cache directories are: /_home/.grogra.de-platform/pluginmngr_cache_plugin/_ and _/home/.grogra.de-platform/pluginmngr_cache_repo/_

## JSON format for plugin data

The metadata used by the plugin is:

- id : The unique id of the plugin. 
- name : The name of the plugin. Defined in plugin.properties
- versions : A list of _version_.
- description (optional)
- helpUrl (optional)
- vendor (optional)
- mainClass (optional)

A _version_ is composed of:
- its version : The version number
- a downloadUrl : The url to download the plugin .jar. It can be a local path.
- depends : A list of other GroIMP plugin required to run the plugin. The plugins are designed by their _id_ and a _version number_
- libs : A list of external libraries required by the plugin. The list must contains the name of the jar used by groimp and an url (or local path) to fetch that jar.

Here is an example of a plugin.json file: 

```xml
[
    {
        "id" : "de.grogra.test",
        "name" : "Test",
        "description" : "This is a testing plugin",
        "helpUrl": "https://gitlab.com/grogra/groimp-plugins/",
        "vendor": "de.grogra",
        "mainClass": "",
        "versions" : {
            "1.2.3" : {
                "downloadUrl" : "https://gitlab.com/grogra/groimp-plugins/Test/download",
                "libs" : {
                "xmlbeans.jar" :"https://repo1.maven.org/maven2/org/apache/xmlbeans/xmlbeans/2.3.0/xmlbeans-2.3.0.jar"
                },
                "depends" : {
                   "de.grogra.ext.x3d":"=2.1.1"
                }
            }
        }
    }
]
```

## Plugin version conditions

### Equal condition
The plugin usually depends on other groimp plugins. It can however depends on a specific version of a plugin. 

That dependency is defined by the **=** before the version number:
```
"depends" : {
    "de.grogra.ext.x3d":"=2.1.1"
}
```

### Higher than condition
By default the plugin manager uses the "Higher than condition". It means that if a plugin depends on another, as long a a version newer of that plugin is available, the condition is met.

## GroIMP commands 

The plugin manager's operations are based on GroIMP commands. Thus, usable without using the GUI (neither the Plugin Manager nor GroIMP GUI are required). 

The list of commands is :

- Exporting the list of currently installed plugins. This create a file with the list of plugin and their version installed. - complete path: _/pluginmanager/commands/file/exportPlugins_. 
- Importing a list of plugins. The plugin manager will install a list of plugins provided by file containing the plugin id and version - complete path: _/pluginmanager/commands/file/importPlugins_.
- Reload the plugin repositories. This refresh the list of available plugins - complete path: _/pluginmanager/commands/data/reloadRepo_.
- Clear the cache. This delete all cached data of both plugin, and repositories - complete path: _/pluginmanager/commands/data/clearCache_.
