package de.grogra.pm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import de.grogra.pf.boot.Main;
import de.grogra.pm.util.CacheUtils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class ChangesMaker {
    private final Map<PluginEntry, Integer> allPlugins;

    public ChangesMaker(Map<PluginEntry, Integer> allPlugins) {
        this.allPlugins = allPlugins;
    }


    public ProcessBuilder getProcessBuilder(File moveFile, File installFile, File restartFile) throws IOException {
        final ArrayList<String> command = new ArrayList<>();
        command.add(SafeDeleter.getJVM());
        command.add("-classpath");
        command.add(URLDecoder.decode(getTempPmgrJAR().getPath(), "UTF-8"));
        command.add(SafeDeleter.class.getCanonicalName());
        command.add("--move-list");
        command.add(moveFile.getAbsolutePath());
        command.add("--install-list");
        command.add(installFile.getAbsolutePath());

        if (restartFile != null) {
            command.add("--restart-command");
            command.add(restartFile.getAbsolutePath());
        }

        Main.getLogger().warning("Command to execute: " + command);
        final ProcessBuilder builder = new ProcessBuilder(command);
        File cleanerLog = File.createTempFile("gipm-cleaner-", ".log");
        builder.redirectError(cleanerLog);
        builder.redirectOutput(cleanerLog);
        return builder;
    }

    private File getTempPmgrJAR() throws IOException {
        String jarPath = URLDecoder.decode(PluginManager.class.getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8");
        if (!jarPath.endsWith(".jar")) {
        	Main.getLogger().warning("Suspicious JAR path detected: " + jarPath);
        	File origDir = new File(jarPath);
            File tempDir = Files.createTempDirectory(origDir.getName()).toFile();
            tempDir.delete();
            FileUtils.copyDirectory(origDir, tempDir);
            return tempDir;
        }
        File origJAR = new File(jarPath);
        File tempJAR = File.createTempFile(origJAR.getName(), ".jar");
        tempJAR.delete();
        Files.copy(origJAR.toPath(), tempJAR.toPath());
        return tempJAR;
    }


    public File getRestartFile(LinkedList<String> additionalGroIMPOptions) throws IOException {
        File file = File.createTempFile("gipm-restart-", ".list");
        try (PrintWriter out = new PrintWriter(file)) {

            out.print(SafeDeleter.getJVM() + "\n");

            RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
            List<String> jvmArgs = runtimeMXBean.getInputArguments();
            for (String arg : jvmArgs) {
                out.print(arg + "\n");
            }

            out.print("-jar\n");

            out.print(getGroIMPStartCommand(additionalGroIMPOptions));

            return file;
        }
    }

    private String getGroIMPStartCommand(LinkedList<String> additionalGroIMPOptions) {
        StringBuilder cmd = new StringBuilder(Main.getProperty(Main.BOOT_PATH) + File.separator + "core.jar\n");
        if (additionalGroIMPOptions != null) {
            for (String option : additionalGroIMPOptions) {
                cmd.append(option).append("\n");
            }
        }
        return cmd.toString();
    }

    public File getInstallFile(Set<PluginEntry> plugins) throws IOException {
    	return File.createTempFile("gipm-installers-", ".list");
    }

    protected String generateLibPath(String libName) throws UnsupportedEncodingException {
        String file = this.getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
        File libPath = new File(file).getParentFile();
        File libPathParent = libPath.getParentFile();
        if (Files.isWritable(libPathParent.getAbsoluteFile().toPath())) {
            return URLDecoder.decode(libPathParent.getAbsolutePath(), "UTF-8") + File.separator + libName;
        }

        return URLDecoder.decode(libPath.getAbsolutePath(), "UTF-8") + File.separator + libName;
    }

    public File getMovementsFile(Set<PluginEntry> deletes, Set<PluginEntry> installs) throws IOException {
        final File file = File.createTempFile("gipm-jar-changes", ".list");
        // if cache is used, deleted plugins are saved to a groimp folder (additionaly to the tmp folder)
        try (PrintWriter out = new PrintWriter(file)) {

            if (!deletes.isEmpty()) {
                File delDir = File.createTempFile("gipm-deleted-plugins-", "");
                delDir.delete();
                delDir.mkdir();
                Main.getLogger().warning("Will move deleted Plugins to directory " + delDir);
                for (PluginEntry plugin : deletes) {
                	if (CacheUtils.isUsingCache()) {
                		try {
							plugin.cachePlugin();
						} catch (IOException | URISyntaxException e) {
							//e.printStackTrace();
						}
                	}
                    File installed = new File(plugin.getInstalledPath());
                    String delTo = delDir + File.separator + plugin.getName();
                    out.print(plugin.getInstalledPath() + "\t" + delTo + "\n");
                }

            }

            for (PluginEntry plugin : installs) {
            	String t = plugin.getLibRepo();
                out.print(plugin.getTempName() + "\t" + plugin.getDestName() + "\n");
                if (t!=null) {
            		out.print(t + "\t" + plugin.getDestName() + "\n");
            	}
            }
            return file;
        }
    }
    
    public void changeDisable(Set<PluginEntry> disablePlugins, Set<PluginEntry> ablePlugins) {
    	for (PluginEntry plugin : disablePlugins) {
    		try {
    			String optName = "/de/grogra/options/disable/options/"+plugin.getID();
    			Main.getPropertyFile().writeOption(optName, "true");
    		} catch(NullPointerException|ClassCastException e ) {
    			
    		}
        }
    	
    	for (PluginEntry plugin : ablePlugins) {
    		try {
    			String optName = "/de/grogra/options/disable/options/"+plugin.getID();
    			Main.getPropertyFile().writeOption(optName, "false");
    		} catch(NullPointerException|ClassCastException e ) {
    			
    		}
        }
    }
    
    /*
     * add paths to the blacklist. If the blacklist already exists, append to it.
     * There is no check of duplicate as duplicate shouldn't happen
     */
    public void blacklist(Set<PluginEntry> delPlugins) throws AccessDeniedException {
    	File blacklistFile = new File (Main.getConfigurationDirectory(), "blacklisted");
    	try (BufferedWriter writer = new BufferedWriter(new FileWriter(blacklistFile, true))) {
			for (PluginEntry p : delPlugins) {
				writer.append(System.lineSeparator() + p.getInstalledPath());
			}
		} catch (IOException e) {
			throw new AccessDeniedException("Couldn't access the blacklist file. Uninstall not possible.");
		} 
    }
}
