package de.grogra.pm.repo;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.pm.GenericCallback;
import de.grogra.pm.PluginManager;
import de.grogra.pm.util.HTTPUtils;
import de.grogra.pm.util.PluginEntryUtils;
import de.grogra.pm.util.RepoSourceUtils;
import de.grogra.util.ProgressMonitor;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class RepoManager extends RepoSource {
	
	private static final Logger log = Logger.getLogger(JSONSource.class.getName());
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
    protected AbstractHttpClient httpClient;
    private List<RepoSource> sources = new ArrayList<RepoSource>();
            
    public RepoManager() {
    }
    
    protected AbstractHttpClient getHTTPClient() {
    	if (httpClient == null) {
    		httpClient = HTTPUtils.getHTTPClient();
    	}
    	return httpClient;
    }
    
    public DownloadResult getJar(final String id, String location, final GenericCallback<String> callback) throws IOException {
        // check if its a local file
    	if (new File(location).isFile()) {
    		return getLocalZip(id, location, callback);
    	}    	
    	URI url = URI.create(location);
        log.info("Downloading: " + url);
        callback.notify("Downloading " + id + "...");
        HttpGet httpget = new HttpGet(url);

        HttpContext context = new BasicHttpContext();
        HttpResponse response = HTTPUtils.execute(httpget, context, getHTTPClient());
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
        	log.severe ("Error downloading url:" + url + " got response code:" 
        				+ response.getStatusLine().getStatusCode());
            EntityUtils.consumeQuietly(response.getEntity());
            throw new IOException(response.getStatusLine().toString());
        }

        HttpEntity entity = response.getEntity();

        File tempFile = File.createTempFile(id.replaceAll("[^a-zA-Z0-9\\._]+", "_").replaceAll("-", "_")+"-", ".jar");


        final long size = entity.getContentLength();

        try (InputStream inputStream = entity.getContent();
             OutputStream fos = new FileOutputStream(tempFile);
             BufferedOutputStream bos = new BufferedOutputStream(fos)) {

            HTTPUtils.copyLarge(inputStream, bos, new GenericCallback<Long>() {
                @Override
                public void notify(Long progress) {
                    callback.notify(String.format("Downloading %s: %d%%", id, 100 * progress / size));
                }
            });
            callback.notify("Downloaded " + id + "...");

            Header cd = response.getLastHeader("Content-Disposition");
            String filename;
            if (cd != null && cd.getValue().contains("filename")) {
                filename = cd.getValue().split(";")[1].split("=")[1];
                if (filename.length() > 2 && filename.startsWith("\"") && filename.endsWith("\"")) {
                    filename = filename.substring(1, filename.length() - 1);
                }
            } else {
                HttpUriRequest currentReq = (HttpUriRequest) context.getAttribute(ExecutionContext.HTTP_REQUEST);
                HttpHost currentHost = (HttpHost) context.getAttribute(ExecutionContext.HTTP_TARGET_HOST);
                String currentUrl = (currentReq.getURI().isAbsolute()) ? currentReq.getURI().toString() : (currentHost.toURI() + currentReq.getURI());
                filename = FilenameUtils.getName(currentUrl);
            }

            return new DownloadResult(tempFile.getPath(), filename);
        }
    }
    
    /** 
     * if location is file -> zip from cache -> copy the repo to tmp and location remains
     * @param id
     * @param location
     * @param statusChanged
     * @return
     * @throws IOException
     */
    public DownloadResult getLocalZip(String id, String location, GenericCallback<String> statusChanged) throws IOException {
    	File orig = new File(location);
    	if (PluginEntryUtils.isZip(orig)) {
    		File tmp = File.createTempFile("gipm-", ".jar");
    		FileUtils.copyFile(orig, tmp);
    		return new DownloadResult(orig.getAbsolutePath(), orig.getName());
    	}
    	File dir = Files.createTempDirectory("gipm-").toFile();
    	FileUtils.copyDirectory(orig.getParentFile(), dir);
    	return new DownloadResult(new File(dir, FilenameUtils.getBaseName(location)).getAbsolutePath(), orig.getParentFile().getName());
	}

 
    /**
     * A repository is added as a URI. return the RepoSource created
     * @param repo
     */
    public RepoSource addRepo(URI repo, boolean active) {
    	RepoSource newRepo = null;
    	if (contained(repo)) {
    		return newRepo;
    	}
    	File f = new File(repo.toString());
		if (f.getName().endsWith(".list")) {
			newRepo = new ListSource(repo).setActive(active);
			sources.add(newRepo);
    	}
		else if (f.isDirectory() && f.exists()) {
			newRepo = new DirectorySource(repo).setActive(active);
			sources.add(newRepo );
		} else {
			newRepo = new JSONSource(repo).setActive(active);
			sources.add( newRepo);
		}
		return newRepo;
    }
    
    public RepoSource addRepo(String repo) {
    	boolean active=RepoSourceUtils.isActiveFromString(repo);
    	String uri;
    	try {
    		if(!active) {
    			uri = repo.substring(1);
    		}
    		else {
    			uri = repo;
    		}
    		URI uriRepo = new URI(uri);
    		return addRepo(uriRepo, active);
    	} catch(URISyntaxException e) {
    		log.info("Repository could not be loaded: " + repo);
    	}
    	return null;    	
    }

    public RepoSource addRepo(URI repo) {
    	boolean active=RepoSourceUtils.isActiveFromString(repo.toString());
    	return addRepo(repo, active);
    }
    
    
    /**
     * 
     */
    public void removeRepo(RepoSource repo) {
    	sources.remove(repo);
    }
    
    /*
     * Return true if the repo is already added to the sources
     */
    public boolean contained(URI repo) {
    	for(RepoSource s : sources ) {
    		if (s.getFile().equals((repo))) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public JSONArray getRepo() throws IOException {
    	return getRepo(getHTTPClient());
    }

	@Override
	public JSONArray getRepo(Object c) throws IOException {
		final JSONArray result = new JSONArray();
        final List<String> pluginsIDs = new ArrayList<>();

        initProgressMonitor	(UI.createProgressAdapter (Workbench.current ()));
        setProgress("Loading plugin information from repositories...", ProgressMonitor.INDETERMINATE_PROGRESS);
        int i=0;
        for (RepoSource source : sources) {
        	if (!source.isActive()) {continue;}
        	try {
        	JSONArray json = source.getRepo(c);
            for (Object elm : json) {
                // resolve plugin-id conflicts 
                String id = ((JSONObject) elm).getString("id");
                if (!pluginsIDs.contains(id)) {
                    pluginsIDs.add(id);
                    result.put(elm);
                } else {
                	// merge the two
                	JSONObject old = getJSONObjectOfId(result, id);
                	if (old!=null) {
                    	JSONObject versions = old.getJSONObject("versions");
                    	for (String newVer : ((JSONObject) elm).getJSONObject("versions").keySet()) {
                    		if (!versions.has(newVer)) {
                    			versions.put(newVer, ((JSONObject) elm).getJSONObject("versions").getJSONObject(newVer));
                    		}
                    		else {
                            	log.info("Plugin " + id + " version : " 
                    		+ newVer + " will be skipped, because it is duplicated.");
                    		}
                    	}
                	}
                }
            }
        	}catch(JSONException e) {
        		log.info("Source at " + source.getFile().toString() + " could not be loaded.");
        	}
        	i++;
            setProgress("Loading plugin information from repositories...", i/sources.size());
        }
        
        setProgress("Repositories loaded.", ProgressMonitor.DONE_PROGRESS);
        return result;
	}

	/**
	 * return the jsonobject whose argument getString("id") is id
	 * return null if no such object is in the array
	 */
	protected JSONObject getJSONObjectOfId(JSONArray json, String id) {
		for (int i=0; i<=json.length();i++) {
			Object elm = json.get(i);
			if (elm instanceof JSONObject && ((JSONObject)elm).getString("id").equals(id)) {
				return (JSONObject) elm;
			}
		}
		return null;
	}
	
	@Override
	public void setTimeout(int timeout) {
	}
	
	
	public void cleanRepo() {
		sources = new ArrayList<RepoSource>();
	}
    
	public List<RepoSource> getSources(){
		return this.sources;
	}
	
	public RepoSource changeFile(RepoSource repo, URI newUri) {
		if (repo.file.toString().equalsIgnoreCase(newUri.toString())) {
			return repo;
		}
    	if ( repo.accept( newUri) ) {
    		repo.file = newUri;
    		return repo;
    	}
    	else {
    		removeRepo(repo);
    		return addRepo(newUri);
    	}
    }
	
	public boolean accept(URI uri) {
		return false;
	}

	public void reloadHTTPClient() {
		httpClient = null;
		httpClient = HTTPUtils.getHTTPClient();
	}
}
