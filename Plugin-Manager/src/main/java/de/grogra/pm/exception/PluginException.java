package de.grogra.pm.exception;

/**
 *  Throws when there are any errors in with the plugin downloaded
 */
public class PluginException extends RuntimeException {
    public PluginException() {
    }

    public PluginException(String message) {
    	super(message, null, false, false);
    }

    public PluginException(String message, Throwable cause) {
        super(message, cause);
    }
}
