package de.grogra.pm.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.commons.io.FileUtils;

import de.grogra.pf.boot.Main;
import de.grogra.pm.repo.RepoSource.DownloadResult;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class SourcesCache extends CacheBase implements Serializable {
    private static final long serialVersionUID = 1L;

    public SourcesCache(String repoJSON, long expirationTime, long lastModified) {
        this.cachedObject = repoJSON;
        this.expirationTime = expirationTime;
        this.lastModified = lastModified;
    }

    public void saveToFile(File file, DownloadResult dwn) throws IOException {
        // Serialization
        try (FileOutputStream fout = new FileOutputStream(file);
             ObjectOutputStream out = new ObjectOutputStream(fout)) {
            FileUtils.touch(file);
            // Method for serialization of object
            out.writeObject(this);
        } catch (IOException ex) {
        	Main.getLogger().warning("Failed for serialize repo"+ ex);
        }
    }

    public static SourcesCache fromFile(File file) {
        // Deserialization
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream in = new ObjectInputStream(fis);) {

            // Method for deserialization of object
            return (SourcesCache) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Main.getLogger().warning("Failed for deserialize repo"+ ex);
            return null;
        }
    }

}
