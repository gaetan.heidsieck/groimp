package de.grogra.pm.cache;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import de.grogra.pm.repo.RepoManager;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public abstract class CacheBase implements Serializable {

	private static final long serialVersionUID = 4104942870027438627L;
	protected String cachedObject; // complete path (/path/to/cache/PLUGINNAME-VER/hex)
    protected long expirationTime;
    protected long lastModified;
    
    public CacheBase() {
    	this.cachedObject = null;
        this.expirationTime = 0;
        this.lastModified = 0;
    }
        
    public boolean isActual() {
    	if (expirationTime==0) {
    		return true;
    	}
    	else {
    		return expirationTime > System.currentTimeMillis();
    	}
    }

    public boolean isActual(long lastModified) {
        return isActual() && lastModified <= this.lastModified;
    }

    public long getExpirationTime() {
        return expirationTime;
    }
    
    public String getCachedObject() {
        return cachedObject;
    }
    
    public abstract void saveToFile(File file, RepoManager.DownloadResult dwn) throws IOException ;

    // Needs to be implemented in extended classes
    public static CacheBase fromFile(File file) {
    	return null;
    }
}
