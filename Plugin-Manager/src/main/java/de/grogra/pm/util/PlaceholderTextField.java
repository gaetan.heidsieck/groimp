package de.grogra.pm.util;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JTextField;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class PlaceholderTextField extends JTextField {

    private String placeholder;

    @Override
    protected void paintComponent(final Graphics pG) {
        super.paintComponent(pG);

        if (placeholder.length() == 0 || getText().length() > 0) {
            return;
        }

        final Graphics2D g = (Graphics2D) pG;
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(getDisabledTextColor());
        g.drawString(placeholder, getInsets().left, pG.getFontMetrics()
                .getMaxAscent() + getInsets().top);
    }

    public void setPlaceholder(final String s) {
        placeholder = s;
    }

    public String getPlaceholder() {
        return placeholder;
    }
}
