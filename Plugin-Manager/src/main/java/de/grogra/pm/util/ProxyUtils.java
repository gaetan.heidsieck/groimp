package de.grogra.pm.util;

import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

public class ProxyUtils {

	/**
	 * Load the options based on: first the Main property 
	 * (given by the user on startup (in cmd line)).
	 * Then on option value of the current workbench.
	 */
	public static String getOption(String name) {
		return getOption(name, null);
	}
	
	public static String getOption(String name, String defaultValue) {
		String result = Main.getProperty(name, null);
		if (result != null) {
			return result;
		}
		Workbench r = Workbench.current();
		if (r == null) { return defaultValue; }
    	Item opt = Item.resolveItem (r.getRegistry(), "/pluginmanager/options/proxy");

		if (opt != null) {
			result = (String) Utils.get (opt, name, defaultValue);
		}
		return result;
	}
}
