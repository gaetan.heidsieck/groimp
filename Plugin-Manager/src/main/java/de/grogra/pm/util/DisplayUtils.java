package de.grogra.pm.util;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import de.grogra.pm.PluginEntry;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class DisplayUtils {

	public static String pluginVersions(PluginEntry plugin) {
		StringBuffer buf = new StringBuffer();
		if (plugin.isInstalled()) {
			buf.append("Plugin installed with version :");
			buf.append(plugin.getInstalledVersion());
		} else {
			buf.append("Plugin not installed");
		}
		buf.append("\n");
		buf.append("Available versions :");
		for(String v : plugin.getVersions()) {
			buf.append(v);
			buf.append(", ");
		}
		buf.append("\n");
		return buf.toString();
	}
	
	public static String pluginListToString(Set<PluginEntry> list){
		StringBuffer buf = new StringBuffer();
		buf.append(" Plugin List : \n");
		buf.append("Name		|	installed version	|	highest available version \n");
		for (PluginEntry p : list) {
			buf.append(pluginToString(p));
			buf.append("\n");
		}
		return buf.toString();
	}
	
	public static String pluginToString(PluginEntry plugin) {
		StringBuffer buf = new StringBuffer();
		buf.append(plugin.getName());
		if (plugin.getName().length()<8) {
			buf.append("		|	");
		} else {
			buf.append("	|	");
		}
		if (plugin.isInstalled()) {
			buf.append(plugin.getInstalledVersion());
			buf.append("			|	");
		}
		else {
			buf.append("Not Installed");
			buf.append("		|	");
		}
		buf.append(plugin.getMaxVersion());
		return buf.toString();
	}
	
	public static String getDescriptionHTML(PluginEntry plugin) {
        String txt = "<h1>" + plugin.getName() + "</h1>";

        if (plugin.isUpgradable()) {
            txt += "<p><font color='orange'>This plugin can be upgraded to version " + plugin.getMaxVersion() + "</font></p>";
        }
        
        if (!plugin.getDescription().isEmpty()) {
            txt += "<p>" + plugin.getDescription() + "</p>";
        }
        
        if (plugin.isDisabled()) {
            txt += "<p> <font color='red'>  The plugin is currently disabled in your GroIMP. </font></p>";
        }
        
        if (!plugin.getHelpLink().isEmpty()) {
            txt += "<p>Documentation: <a href='" + plugin.getHelpLink() + "'>" + plugin.getHelpLink() + "</a></p>";
        }
        String changes = plugin.getVersionChanges(plugin.getCandidateVersion());
        if (null != changes) {
            txt += "<p>What's new in version " + plugin.getCandidateVersion() + ": " + changes + "</p>";
        }
//        txt += getMavenInfo(plugin);
        if (plugin.getInstalledPath() != null) {
            txt += "<pre>Current location: " + plugin.getInstalledPath() + "</pre>";
        }
        if (plugin.getTempName() != null) {
            txt += "<pre>Get from : " + plugin.getTempName() + "</pre>";
        }

        Map<String, String> deps = plugin.getDepends();
        if (!deps.isEmpty()) {
            txt += "<pre>Dependencies: " + Arrays.toString(deps.keySet().toArray(new String[0])) + "</pre>";
        }
        Set<String> excls = plugin.getExcluded();
        if (!excls.isEmpty()) {
            txt += "<pre>Exclusions: " + Arrays.toString(excls.toArray(new String[0])) + "</pre>";
        }
        Map<String, String> libs = plugin.getLibs(plugin.getCandidateVersion());
        if (!libs.isEmpty()) {
            txt += "<pre>Libraries: " + Arrays.toString(libs.keySet().toArray(new String[0])) + "</pre>";
        }
        if (!plugin.getVendor().isEmpty()) {
            txt += "<p>Vendor: <i>" + plugin.getVendor() + "</i></p>";
        }

        return txt + "<br/>";
    }
}
