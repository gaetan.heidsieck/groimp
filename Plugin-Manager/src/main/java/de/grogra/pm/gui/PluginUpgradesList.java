package de.grogra.pm.gui;


import de.grogra.pm.GenericCallback;
import de.grogra.pm.PluginEntry;
import de.grogra.pm.gui.PluginCheckbox.StateListener;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class PluginUpgradesList extends PluginsList {
    /**
     *
     */
    private static final long serialVersionUID = 525391154129274758L;

    public PluginUpgradesList(GenericCallback<Object> dialogRefresh, boolean canHalfstate) {
        super(dialogRefresh, canHalfstate);
    }

    @Override
    protected PluginCheckbox getCheckboxItem(PluginEntry plugin, StateListener changeNotifier) {
        PluginCheckbox checkboxItem = super.getCheckboxItem(plugin, changeNotifier);
        plugin.setCandidateVersion(plugin.getMaxVersion());
        checkboxItem.setSelectedVersion(plugin.getMaxVersion());
        checkboxItem.setSelected(false);
        return checkboxItem;
    }

    @Override
    protected void setUpVersionsList(PluginCheckbox cb) {
        super.setUpVersionsList(cb);
        version.setEnabled(false);
    }

    @Override
    protected String getCbVersion(PluginCheckbox cb) {
        if (cb.isSelected()) {
            return cb.getPlugin().getMaxVersion();
        } else {
            return cb.getPlugin().getInstalledVersion();
        }
    }
}
