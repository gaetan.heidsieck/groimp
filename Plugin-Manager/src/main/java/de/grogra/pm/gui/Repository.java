package de.grogra.pm.gui;

import de.grogra.pm.repo.RepoSource;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class Repository {

	/*
	 * Possible types : CACHE, MAIN, USER
	 */
	public final static int CACHE=0;
	public final static int MAIN=1;
	public final static int USER=2;
	private int type;
	private String value;
	private RepoSource source; //checkbox to activate or deactivate
	
	public Repository (int type, String value, RepoSource source) {
		this.type =type;
		this.value = value;
		this.source=source;
	}
	
	public int getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
	public void setType(int type) {
		this.type = type;
	}
	public void setValue(String value) {
		this.value=value;
	}
	
	public RepoSource getSource() {
		return this.source;
	}
	public void setSource(RepoSource source) {
		this.source=source;
	}
	public boolean getActive() {
		if (source==null) return false;
		return this.source.isActive();
	}
	public void setActive(boolean active) {
		if (source==null) return ;
		this.source.setActive(active);
	}
}
