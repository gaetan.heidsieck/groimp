package de.grogra.pm;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

public class Version {
	
	public interface VersionListener extends EventListener{
		public void versionChanged(VersionListener origin);
		public String getVersion();
	}
		
	private String value;
	private List<VersionListener> listeners = new ArrayList<VersionListener>();

	public Version(String val) {
		this.value = val;
	}
    public void addListener(VersionListener toAdd) {
    	if (!listeners.contains(toAdd))
    		this.listeners.add(toAdd);
    }
    public void removeListener(VersionListener toRemove) {
    	this.listeners.remove(toRemove);
    }
    public void removeAllListeners() {
    	this.listeners.clear();
    }
    public List<VersionListener> getListeners(){
    	return this.listeners;
    }
	
	public String getValue() {
		return this.value;
	}
	
	public void setValue(String val) {
		if(this.value == val) return;
		this.value=val;
		this.listeners.forEach(l -> l.versionChanged(null));
	}
	
	public void setValue(String val, VersionListener origin) {
		if(this.value == val) return;
		this.value=val;
		this.listeners.forEach(l -> l.versionChanged(origin));
	}
}
