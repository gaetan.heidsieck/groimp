package de.grogra.pm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.grogra.pm.exception.PluginException;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class DependencyResolver {
    private static final Pattern conditionParser = Pattern.compile("^([=<>]+)");
    protected final boolean reload;
    protected final Set<PluginEntry> deletions = new HashSet<>();
    protected final Set<PluginEntry> toDisable = new HashSet<>();
    protected final Set<PluginEntry> toAble = new HashSet<>();
    protected final Set<PluginEntry> additions = new HashSet<>();
    protected final Set<PluginEntry> unchanged = new HashSet<>();
    protected final Map<PluginEntry, Integer> allPlugins;
    protected final Map<PluginEntry, PluginEntry> pluginExclusions = new HashMap<>();

    public DependencyResolver(Map<PluginEntry, Integer> allPlugins) {
        this.allPlugins = allPlugins;
        this.reload=true;
        resolve();
    }
    
    public DependencyResolver(Map<PluginEntry, Integer> allPlugins, boolean reload) {
        this.allPlugins = allPlugins;
        this.reload=reload;
        resolve();
    }

    public void resolve() {
        clear();
        resolveFlags();
        resolveUpgrades();
        resolveDeleteByDependency();
        resolveDisableByDependency();
        resolveAbleByDependency();
        resolveInstallByDependency();
        resolveExclusions();
        detectConflicts();
    }

    public void clear() {
        deletions.clear();
        additions.clear();
        toDisable.clear();
        toAble.clear();
        unchanged.clear();
        allPlugins.forEach((k,v)->k.resetVersionsCandidate(reload));
    }


    public Set<PluginEntry> getDeletions() {
        return deletions;
    }

    public Set<PluginEntry> getAdditions() {
        return additions;
    }
    
    public Set<PluginEntry> getToDisable() {
        return toDisable;
    }

    public Set<PluginEntry> getToAble() {
        return toAble;
    }
    
    public Map<PluginEntry, PluginEntry> getPluginExclusions() {
        return pluginExclusions;
    }
    
    public Set<PluginEntry> getUnchanged() {
        return unchanged;
    }

    private PluginEntry getPluginByID(String id) {
        for (PluginEntry plugin : allPlugins.keySet()) {
            if (plugin.getID().equals(id)) {
                return plugin;
            }
        }
        throw new PluginException("Plugin not found by ID: " + id);
    }

    private Set<PluginEntry> getDependants(PluginEntry plugin) {
        Set<PluginEntry> res = new HashSet<>();
        for (PluginEntry pAll : allPlugins.keySet()) {
            for (String depID : pAll.getDepends().keySet()) {
                if (depID.equals(plugin.getID())) {
                    res.add(pAll);
                }
            }
        }
        return res;
    }
    
    /*
     * return true if at least one of the installed plugin exclude the plugin
     */
    private boolean isExcluded(PluginEntry plugin) {
    	Set<PluginEntry> installedPlugins = PluginManager.getInstalledPlugins(allPlugins);
    	for (PluginEntry p : installedPlugins) {
            if (p.getExcluded().contains(plugin.getID())) {
            	return true;
            }
    	}
    	return false;
    }


    private void resolveFlags() {
        for (Map.Entry<PluginEntry, Integer> entry : allPlugins.entrySet()) {
            if (entry.getKey().isInstalled()) {
                if (entry.getValue().equals(PluginManager.UNINSTALL)) {
                    deletions.add(entry.getKey());
                }
                else if (!entry.getKey().isDisabled() 
                		&& entry.getValue().equals(PluginManager.DISABLE)) {
                    toDisable.add(entry.getKey());
                }
                else if (entry.getKey().isDisabled() 
                		&& entry.getValue().equals(PluginManager.INSTALL)) {
                	toAble.add(entry.getKey());
                }
            } else if (entry.getValue().equals(PluginManager.INSTALL)) {
                additions.add(entry.getKey());
            }
        }
    }

    private void resolveUpgrades() {
        // detect upgrades
        for (Map.Entry<PluginEntry, Integer> entry : allPlugins.entrySet()) {
        	PluginEntry plugin = entry.getKey();
            if (entry.getValue().equals(PluginManager.INSTALL) && plugin.isInstalled() 
            		&& !plugin.getInstalledVersion().equals(plugin.getCandidateVersion())) {
                deletions.add(plugin);
                additions.add(plugin);
            }
        }
    }

    //TODO: check if another version of a plugin should be installed -> delete any other
    private void resolveDeleteByDependency() {
        // delete by depend
        boolean hasModifications = true;
        while (hasModifications) {
            hasModifications = false;
            for (PluginEntry plugin : deletions) {
            		if (!additions.contains(plugin)) {
	                    for (PluginEntry dep : getDependants(plugin)) {
	                        if (!deletions.contains(dep) && dep.isInstalled()) {
	                            deletions.add(dep);
	                            hasModifications = true;
	                        }
	                        if (additions.contains(dep)) {
	                            additions.remove(dep);
	                            hasModifications = true;
	                        }
	                    }
	                }
	
	                if (hasModifications) {
	                    break; // prevent ConcurrentModificationException
	                }
            }
        }
    }
    
    
    private void resolveDisableByDependency() {
        // disable by depend
        boolean hasModifications = true;
        while (hasModifications) {
            hasModifications = false;
            for (PluginEntry plugin : toDisable) {
            		if (!toAble.contains(plugin) && !deletions.contains(plugin)) {
	                    for (PluginEntry dep : getDependants(plugin)) {
	                        if (!toDisable.contains(dep) 
	                        		&& ( !dep.isDisabled() && dep.isInstalled())) {
	                        	toDisable.add(dep);
	                            hasModifications = true;
	                        }
	                        if (toAble.contains(dep)) {
	                            toAble.remove(dep);
	                            hasModifications = true;
	                        }
	                    }
	                }
	
	                if (hasModifications) {
	                    break; // prevent ConcurrentModificationException
	                }
            }
        }
    }    
    
    private void resolveExclusions() {
        // resolve exclusions
        boolean hasModifications = true;
        while (hasModifications) {
            hasModifications = false;
            for (PluginEntry plugin : additions) {
            	for (String pluginID : plugin.getExcluded()) {
            		PluginEntry excl = getPluginByID(pluginID);
            		if ( (excl.isInstalled() && !deletions.contains(excl)) 
            				|| (additions.contains(excl)) ) {
            			additions.remove(plugin);
            			pluginExclusions.put(plugin, excl);
            			hasModifications = true;
            			for (PluginEntry dep : getDependants(plugin)) {
            				if (additions.contains(dep)) {
                                additions.remove(dep);
                                pluginExclusions.put(dep, excl);
                            }
            			}
            		}
            	}
            	if (isExcluded(plugin) ) {
            		additions.remove(plugin);
            		pluginExclusions.put(plugin, null);
            		hasModifications = true;
            	}
                if (hasModifications) {
                    break; // prevent ConcurrentModificationException
                }
            }
        }
    }
    
    private void resolveInstallByDependency() {
        // resolve dependencies
        boolean hasModifications = true;
        while (hasModifications) {
            hasModifications = false;
            for (PluginEntry plugin : additions) { //p key is id and value is version
                for (Entry<String, String> p : plugin.getDepends().entrySet()) {
                	PluginEntry depend = getPluginByID(p.getKey());
                	String version = getVersionWithoutCondition(p.getValue());
                	String condition = getCondition(p.getValue());
                    if (!depend.isInstalled() 
                    		|| (depend.isInstalled() && depend.isDisabled())
                    		|| deletions.contains(depend)
                    		|| toDisable.contains(depend)
                    		|| !depend.isConditionRespected(condition, version, true)
                    	) {
                    	if (! depend.hasVersion(condition, version))
                    	{
                    		additions.remove(plugin);
                    		throw new PluginException(PluginManager.I18N.msg("error.plugin.requirements.notavailable", 
                    				plugin.getName(), depend.getName(), version)); 
                    	}
                    	hasModifications = processReAble(depend, version, condition);
                    	if (!hasModifications) {
                    		hasModifications = processUpgrade(depend, version, condition);
                    	}                   
                    	}
                }

                if (hasModifications) {
                    break; // prevent ConcurrentModificationException
                }
            }
        }
    }
    
    private void resolveAbleByDependency() {
        boolean hasModifications = true;
        while (hasModifications) {
            hasModifications = false;
            for (PluginEntry plugin : toAble) { //p key is id and value is version
                for (Entry<String, String> p : plugin.getDepends().entrySet()) {
                	PluginEntry depend = getPluginByID(p.getKey());
                	String completeVersion = ( !p.getValue().isEmpty() ) ? p.getValue() : depend.getMaxVersion();
                	String version = getVersionWithoutCondition(completeVersion);
                	String condition = getCondition(completeVersion);
                	// if ( depend do not meet the requirement)
                    if ( !depend.isInstalled()
                    		|| (depend.isInstalled() && depend.isDisabled())
                    		|| deletions.contains(depend)
                    		|| toDisable.contains(depend)
                    		|| !depend.isConditionRespected(condition, version, true)
                    	) {
                    	// if ( depend has no version to meet the requirement)
                    	if (! depend.hasVersion(condition, version))
                    	{
                    		toAble.remove(plugin);
                    		throw new PluginException(PluginManager.I18N.msg("error.plugin.requirements.notavailable", 
                    				plugin.getName(), depend.getName(), version)); 
                    	}
                    	// if (depend should only be re able)
                    	hasModifications = processReAble(depend, version, condition);
                    	if (!hasModifications) {
                    		// else depend will be upgraded (i.e. removed and installed with a newer version)
                    		hasModifications = processUpgrade(depend, version, condition);
                    	}
                    	}
                }

                if (hasModifications) {
                    break; // prevent ConcurrentModificationException
                }
            }
        }
    }
    
    /**
     * Re able the dependency if required - return true if some change have been done
     */
    protected boolean processReAble(PluginEntry depend, String version, String condition) {
    	if (toAble.contains(depend)) {
    		return false;
    	}
    	if (depend.isInstalled() 
    			&& ( depend.isDisabled() || toDisable.contains(depend))
    			&& depend.isConditionRespected(condition, version, true)) {
    		toAble.add(depend);
    		return true;
    	}
    	return false;
    }
    
    /**
     * Add the dependency version if required - return true if some change have been done
     */
    protected boolean processUpgrade(PluginEntry depend, String version, String condition) {
    	if (depend.isInstalled()) {
    		deletions.add(depend);
    	}
    	if (!depend.isConditionRespected(condition, version, false)) {
   			depend.setCandidateVersion(condition, version);
        }
        if (!additions.contains(depend)) {
            additions.add(depend);
            return true;
        }
    	return false;
    }

    protected String getVersionWithoutCondition(String version) {
    	Matcher m = conditionParser.matcher(version);
    	if (m.find()) {
    		return conditionParser.split(version)[1];
    	}
    	return version;
    }
    
    protected String getCondition(String version) {
    	Matcher m = conditionParser.matcher(version);
    	if (m.find()) {
    		return m.group(1);
    	}
    	return "";
    }
    
    // TODO: manage '==' and '<=' condition
    protected void verifyConditionFormat(String condition) {
        if (!condition.equals(">=")) {
            throw new IllegalArgumentException(PluginManager.I18N.msg("", condition));
        }
    }

    /**
     * If a plugin is both in delete and addition && the installed version == candidate version - then
     * remove plugin from both of theses sets
     * If a plugin is to be disabled but is also to be deleted. It is not disabled
     */
    public void detectConflicts() {
    	Set<PluginEntry> tmp = new HashSet<>();
    	for (PluginEntry plugin : additions) {
    		if (deletions.contains(plugin)) {
    			if (plugin.getCandidateVersion().contentEquals(plugin.getInstalledVersion())) {
    				unchanged.add(plugin);
    			}
    		}
    	}
    	for (PluginEntry plugin : unchanged) {
    		additions.remove(plugin);
    		deletions.remove(plugin);
    	}
    	
    	for (PluginEntry plugin : toDisable) {
    		if (deletions.contains(plugin)) {
    			if (plugin.getCandidateVersion().contentEquals(plugin.getInstalledVersion())) {
        			tmp.add(plugin);
    			}
    		}
    	}
    	for (PluginEntry plugin : tmp) {
    		toDisable.remove(plugin);
    	}
    }
}
