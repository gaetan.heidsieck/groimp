module numeric {
	exports de.grogra.numeric.cvode;
	exports de.grogra.numeric;

	requires com.sun.jna;
	requires commons.math3;
}