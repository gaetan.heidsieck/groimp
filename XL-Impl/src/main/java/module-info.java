module xl.impl {
	exports de.grogra.xl.impl.dom;
	exports de.grogra.xl.impl.base;
	exports de.grogra.xl.impl.simple;
	exports de.grogra.xl.impl.queues;
	exports de.grogra.xl.impl.property;

	requires utilities;
	requires xl;
	requires xl.core;
	requires java.desktop;
	requires java.xml;
}