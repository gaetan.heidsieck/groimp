package de.grogra.pf.registry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * The abstract class that define the registry items stored in the /statics directory
 */
public abstract class ContentDescriptionType extends Item implements Comparable<ContentDescriptionType>{

	public static class Parameter {
		public String description;
		public String fullType;
		public String type;
		public String name;
		public boolean isArray;
		public int depth=0; // amount of array depth
		public Parameter[] children; // if the type is a list, the child is the type of the list
		public String valueAsString;

		Parameter (String name, String type, String fullType, String description) {
			this(name, type, fullType, description, false, null);
		}
		
		Parameter (String name, String type, String fullType, String description, boolean isArray) {
			this(name, type, fullType, description, isArray, null);
		}
		
		Parameter (String name, String type, String fullType, String description, 
				boolean isArray, Parameter[] children) {
			this.name=name;
			this.type=type;
			this.fullType=fullType;
			this.description=description;
			this.isArray=isArray;
			this.children=children;
		}
		
		Parameter () {
			this.name="";
			this.type="";
			this.fullType="";
			this.description="";
			this.isArray=false;
		}
		
		@Override
		public String toString() {
			StringBuffer s = new StringBuffer();
			s.append((fullType.isBlank()?type:fullType));
			Iterator<Parameter> childs = Arrays.stream(children).iterator();
			if (childs.hasNext()) {
				s.append("<");
				while (childs.hasNext()) {
					Parameter child = childs.next();
					s.append(child.type);
					if (childs.hasNext()) {
						s.append(",");
					}
				}
				s.append(">");
			}
			s.append(getBrackets());
			return s.toString();
		}
		
		public String toString(boolean simple) {
			StringBuffer s = new StringBuffer();
			s.append(((fullType.isBlank() || simple)?type:fullType));
			Iterator<Parameter> childs = Arrays.stream(children).iterator();
			if (childs.hasNext()) {
				s.append("<");
				while (childs.hasNext()) {
					Parameter child = childs.next();
					s.append(child.type);
					if (childs.hasNext()) {
						s.append(",");
					}
				}
				s.append(">");
			}
			s.append(getBrackets());
			return s.toString();
		}
		
		public String getBrackets() {
			StringBuffer buf = new StringBuffer();
			for (int i=0; i<depth;i++) {
				buf.append("[]");
			}
			return buf.toString();
		}
	}
	
	protected String t_name;
	protected String t_visibility;
	protected boolean t_static;
	protected boolean t_final;
	protected boolean t_abstract;
	protected String t_class;
	protected String t_package;
	protected Parameter t_return;
	protected Class t_clazz;
	protected final ArrayList<String> t_description;
	protected final ArrayList<String> t_see;
	protected final ArrayList<Parameter> t_parameters;
	
	protected boolean t_isEnclosed;
	protected ArrayList<ContentDescriptionType> t_superClasses;
	protected ArrayList<ContentDescriptionType> t_constructors;
	protected ArrayList<ContentDescriptionType> t_internalTypes; // both methods and fields
	
	public ContentDescriptionType() {
		super(null);
		t_name = "";
		t_visibility = "";
		t_static = false;
		t_final = false;
		t_abstract = false;
		t_class = "";
		t_package = "";
		t_return = null;

		t_description = new ArrayList<String>();
		t_see = new ArrayList<String>();
		t_parameters = new ArrayList<Parameter>();
		
		t_isEnclosed = false;
		t_superClasses = new ArrayList<ContentDescriptionType>();
		t_constructors = new ArrayList<ContentDescriptionType>();
		t_internalTypes = new ArrayList<ContentDescriptionType>(); // both methods and fields


		t_clazz = null;
	}

	public String getTName() {
		return t_name;
	}

	public void setTName(String t_name) {
		this.t_name = t_name;
		setName(t_name);
	}

	public String getTVisibility() {
		return t_visibility;
	}

	public void setTVisibility(String t_visibility) {
		this.t_visibility = t_visibility;
	}

	public boolean isTStatic() {
		return t_static;
	}

	public void setTStatic(boolean t_static) {
		this.t_static = t_static;
	}

	public boolean isTFinal() {
		return t_final;
	}

	public void setTFinal(boolean t_final) {
		this.t_final = t_final;
	}

	public boolean isTAbstract() {
		return t_abstract;
	}

	public void setTAbstract(boolean t_abstract) {
		this.t_abstract = t_abstract;
	}

	public String getTClass() {
		return t_class;
	}

	public void setTClass(String t_class) {
		this.t_class = t_class;
	}

	public String getTPackage() {
		return t_package;
	}

	public void setTPackage(String t_package) {
		this.t_package = t_package;
	}

	public Parameter getTReturn() {
		return t_return;
	}

	public void setTReturn(Parameter t_return) {
		this.t_return = t_return;
	}

	public Class getTClazz() {
		return t_clazz;
	}

	public void setTClazz(Class t_clazz) {
		this.t_clazz = t_clazz;
	}

	public void addTDescription(String description)
	{
		t_description.add(description);
	}

	public void addTParameter(Parameter param)
	{
		t_parameters.add(param);
	}
	
	public void addTSee(String param)
	{
		t_see.add(param);
	}
	
	public ArrayList<String> getTDescription() {
		return t_description;
	}
	
	public ArrayList<Parameter> getTParameters() {
		return t_parameters;
	}
	
	public ArrayList<String> getTSee() {
		return t_see;
	}
	
	public boolean isTEnclosed() {
		return t_isEnclosed;
	}
	
	public void setTEnclosed(boolean b) {
		this.t_isEnclosed=b;
	}
	
	public ArrayList<ContentDescriptionType> getTSuperClasses() {
		return t_superClasses;
	}
	
	public void addTSuperClass(ContentDescriptionType c) {
		this.t_superClasses.add(c);
	}
	
	public ArrayList<ContentDescriptionType> getTConstructors() {
		return t_constructors;
	}
	
	public void addTConstructors(ContentDescriptionType c) {
		t_constructors.add(c);
	}
	
	public ArrayList<ContentDescriptionType> getTInternalTypes() {
		return t_constructors;
	}
	
	public void addTInternalTypes(ContentDescriptionType c) {
		t_constructors.add(c);
	}	
	
	/**
	 * Parse a full method name and set the package name and class name.
	 * E.g. of input: de.grogra.Class#method(Var1 )
	 */
	public void parsePackage (String fullName) {
		String partName = fullName.split("#")[0];
		int i = partName.lastIndexOf('.');
		String[] n =  {partName.substring(0, i), partName.substring(i+1)};
		setTPackage(n[0]);
		setTClass(n[1]);
	}
	
	public abstract boolean shouldBeAddedToRegistry();
	public abstract boolean shouldBeAddedToStaticParser();
}
