/*
 * Copyright (C) 2013 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA02111-1307, USA.
 */

package de.grogra.pf.registry;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import de.grogra.pf.registry.ContentDescriptionType.Parameter;
import de.grogra.xl.util.ObjectList;


/**
 * Parses the method xml description file (Platfor-Core/jel.xml) and returns a object representation of it.
 * 
 * @author mh, yong
 * changed for new doclet
 *
 */
public class ContentDescriptionXMLReader {

	public static final String XML_TAG_METHOD					= "method";
	public static final String XML_TAG_METHOD_SEE 				= "see";
	public static final String XML_TAG_METHOD_DESCRIPTION		= "description";
	public static final String XML_TAG_METHOD_PURPOSE		= "purpose";
	public static final String XML_TAG_METHOD_PARAMETER			= "parameter";
	public static final String XML_TAG_METHOD_RETURN 		= "return";
	public static final String XML_TAG_FIELD 				= "field";
	public static final String XML_TAG_CLASS 				= "class";
	public static final String XML_TAG_TYPE				= "type";
	public static final String XML_TAG_ARRAY				= "array";

	public static final String XML_TAG_CLASS_CONSTRUCTOR = "constructor";
	public static final String XML_TAG_CLASS_INTERFACE = "interface";
	public static final String XML_TAG_CLASS_INTERFACES = "interfaces";
	public static final String XML_TAG_CLASS_SUPERCLASS = "superclass";

	public static final String XML_TAG_PACKAGE= "package";
	public static final String XML_TAG_CLASSREF= "classref";
	public static final String XML_TAG_INTERFACEREF= "interfaceref";

	public static final int NOTREADING = 0;
	public static final int METHOD = 1;
	public static final int FIELD = 2;
	public static final int CLASS = 4;
	public static final int CONSTRUCTOR = 8;

	static boolean emptyClass = true;
	static boolean staticOrprivateClass = false;
	static boolean emptyMethod = true;
	static boolean emptyField = true;

	static String tmpValue;


	/**
	 * @param is
	 * @return
	 */
	public static HashMap<String, ContentDescriptionType[]> readXML(InputStream is) {

		int currentlyReading=NOTREADING;
		//list of content objects
		ArrayList<ContentDescriptionType> methodList = new ArrayList<ContentDescriptionType>();
		ArrayList<ContentDescriptionType> classList = new ArrayList<ContentDescriptionType>();
		ArrayList<ContentDescriptionType> fieldList = new ArrayList<ContentDescriptionType>();

		XMLInputFactory factory = XMLInputFactory.newInstance();
		ObjectList<ContentDescriptionType> stack = new ObjectList<ContentDescriptionType>();

		XMLEventReader reader=null;
		try {
			reader = factory.createXMLEventReader(is);

			ContentDescriptionType content=null;

			ContentDescriptionType toAddT;
			ContentDescriptionType.Parameter toAdd;
			String toAddS;

			while (reader.hasNext()) {
				XMLEvent e = reader.nextEvent();
				//ignore spaces
				if (e.isCharacters() && (e.asCharacters()).isWhiteSpace()) continue;

				//start tags
				if(e.isStartElement()) {
					//if start tag is <method>
					if(isStartElementMethod(e)) {
						content = new ContentDescriptionMethod();
						loadContent((ContentDescriptionMethod)content, e.asStartElement ());
						methodList.add(content);
						stack.push(content);
						currentlyReading += METHOD;
					} 
					else if (isStartElementField(e)) {
						content = new ContentDescriptionField();
						tmpValue=null;
						loadContent((ContentDescriptionField) content, e.asStartElement ());
						fieldList.add(content);
						stack.push(content);
						currentlyReading += FIELD;
					}
					else if (isStartElementClass(e)) {
						if (staticOrprivateClass) {
							// go to the end of that class
							discardStaticOrPrivateClass(reader, e);
							continue;
						}
						content = new ContentDescriptionClass();
						loadContent((ContentDescriptionClass) content, e.asStartElement ());
						classList.add(content);
						stack.push(content);
						currentlyReading += CLASS;
					}
					else if (isStartElementConstructor(e)) {
						content = new ContentDescriptionConstructor();
						loadContent((ContentDescriptionConstructor) content, e.asStartElement ());
						content.setTName(stack.peek(1).getTName());
						stack.push(content);
						currentlyReading += CONSTRUCTOR;
					}
					else if (currentlyReading>NOTREADING) {
						switch(getStartElementName(e)) {
						case XML_TAG_METHOD_SEE:
							toAddS = readSee(reader, e); // this get to the endElement of "see"
							stack.peek(1).addTSee(toAddS);
							break;
						case XML_TAG_METHOD_PURPOSE:
							toAddS = readPurpose(reader, e); // this get to the endElement of "purpose"
							stack.peek(1).addTDescription(toAddS);
							break;							
						case XML_TAG_METHOD_DESCRIPTION:
							toAddS = readDescription(reader, e); // this get to the endElement of "description"
							stack.peek(1).addTDescription(toAddS);
							break;
						case XML_TAG_CLASS_SUPERCLASS:
							toAddT = readSuperClass(reader, e);
							stack.peek(1).addTSuperClass(toAddT);
							break;
						case XML_TAG_CLASS_INTERFACES:
							toAddT = readInterfaces(reader, e);
							for(ContentDescriptionType t : toAddT.getTSuperClasses() ) {
								stack.peek(1).addTSuperClass(t);
							}
							break;
						case XML_TAG_METHOD_PARAMETER:
							toAdd = readParameter(reader, e); // this get to the endElement of "description"
							stack.peek(1).addTParameter(toAdd);
							break;
						case XML_TAG_METHOD_RETURN:
							toAdd = readReturn(reader, e); // this get to the endElement of "description"
							stack.peek(1).setTReturn(toAdd);
							break;
						case XML_TAG_TYPE:
							toAdd = readType(reader, e);
							stack.peek(1).setTReturn(toAdd);
							break;
						case XML_TAG_ARRAY:
							toAdd = readArray(reader, e);
							stack.peek(1).setTReturn(toAdd);
							break;
						}
					}					
					//
				} else if(e.isEndElement()) {
					if(isEndElementMethod(e) || isEndElementClass(e) 
							|| isEndElementField(e) || isEndElementConstructor(e)) {
						ContentDescriptionType last = stack.pop();
						currentlyReading -= checkTypeBasedOnElement(e, last);
						if (last instanceof ContentDescriptionField) {
							// look if the field can be mapped to a class
							findClassName:
								for (int i = 1; i <=stack.size; i++) {
									ContentDescriptionType cl = stack.peek(i);
									if (cl instanceof ContentDescriptionClass) {
										last.setTClass( cl.getTName());
										break findClassName;
									}
									if (cl instanceof ContentDescriptionMethod) {
										last.setTName( cl.getTClass());
										break findClassName;
									}
								}
						}
						else if (last instanceof ContentDescriptionConstructor) {
							stack.peek(1).addTConstructors(last);
						}
						if(currentlyReading<NOTREADING) {
							throw new Exception("Incorrect xml syntax.");
						}
					}
				}
			}
		} catch (Throwable t) {
			return null;
		}
		finally {
			if(reader!=null)
				try {
					reader.close();
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
		}

		HashMap<String, ContentDescriptionType[]> result =  new HashMap<String, ContentDescriptionType[]>();
		result.put("methods", methodList.toArray(new ContentDescriptionType[0]));
		result.put("fields", fieldList.toArray(new ContentDescriptionType[0]));
		result.put("classes", classList.toArray(new ContentDescriptionType[0]));
		return result;
	}


	/**
	 * Return the string element of a See element. This read the event until the end tag.
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static String readSee(XMLEventReader reader, XMLEvent e) throws XMLStreamException {
		String see = "";
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_METHOD_SEE)) {
			e = reader.nextEvent();
			if (e.isStartElement() && getStartElementName(e).equals("span")) {
				Iterator iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("href")) {
						see = attribute.getValue();
					}
				}
			}
		}
		return see;
	}


	/**
	 * Return the string element of a Purpose element. This read the event until the end tag.
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static String readPurpose(XMLEventReader reader, XMLEvent e) throws XMLStreamException {
		StringBuffer purp = new StringBuffer();
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_METHOD_PURPOSE)) {
			e = reader.nextEvent();
			if (e.isStartElement() && getStartElementName(e).equals("body")) {
				while (reader.hasNext() && 
						!(e.isEndElement() && getEndElementName(e).equals("body"))) {
					if (e.isCharacters()) {
						purp.append (e.asCharacters().getData());
					}
					e = reader.nextEvent();
				}
			}
		}
		return purp.toString();
	}

	/**
	 * Return the string element of a Description element. This read the event until the end tag.
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static String readDescription(XMLEventReader reader, XMLEvent e) throws XMLStreamException {
		StringBuffer desc = new StringBuffer();
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_METHOD_DESCRIPTION)) {
			e = reader.nextEvent();
			if (e.isStartElement() && getStartElementName(e).equals("body")) {
				while (reader.hasNext() && 
						!(e.isEndElement() && getEndElementName(e).equals("body"))) {
					if (e.isCharacters()) {
						desc.append (e.asCharacters().getData());
					}
					e = reader.nextEvent();
				}
			}
		}
		return desc.toString();
	}

	//	private static 

	/**
	 * Return the string element of a Parameter element. This read the event until the end tag.
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static ContentDescriptionType.Parameter readParameter(XMLEventReader reader, XMLEvent e) 
			throws XMLStreamException {
		ContentDescriptionType.Parameter param = new ContentDescriptionType.Parameter();
		ArrayList<ContentDescriptionType.Parameter> childrenParam = 
				new ArrayList<ContentDescriptionType.Parameter>();
		Iterator iterator = ((StartElement)e).getAttributes();
		ContentDescriptionType.Parameter c=null;

		while (iterator.hasNext()) {
			Attribute attribute = (Attribute) iterator.next();
			QName name = attribute.getName();
			if(name.toString ().equals ("name")) {
				param.name = attribute.getValue();
			}
		}
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_METHOD_PARAMETER)) {
			e = reader.nextEvent();
			if (isStartElementof(e, XML_TAG_METHOD_PURPOSE)) {
				param.description = readPurpose(reader, e);
			}
			else if (isStartElementof(e, XML_TAG_TYPE)) {
				iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("name")) {
						param.type = attribute.getValue();
					}
					if(name.toString ().equals ("fullname")) {
						param.fullType = attribute.getValue();
					}
				}
			}
			else if (isStartElementof(e,"param")) {
				ContentDescriptionType.Parameter child = new ContentDescriptionType.Parameter();
				iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("name")) {
						child.type = attribute.getValue();
					}
					if(name.toString ().equals ("fullname")) {
						child.fullType = attribute.getValue();
					}
				}
				childrenParam.add(child);
			}
			else if (isStartElementof(e, XML_TAG_ARRAY)) {
				param.isArray = true;
				param.depth= 1;
				while ( reader.hasNext() && !isEndElementof(e, XML_TAG_ARRAY) ) {
					e = reader.nextEvent();
					if (e.isStartElement() && getStartElementName(e).equals("component")) {
						iterator = ((StartElement)e).getAttributes();
						while (iterator.hasNext()) {
							Attribute attribute = (Attribute) iterator.next();
							QName name = attribute.getName();
							if(name.toString ().equals ("name")) {
								param.type = attribute.getValue();
							}
							if(name.toString ().equals ("fullname")) {
								param.fullType = attribute.getValue();
							}
						}

					}
					else if (isStartElementof(e, XML_TAG_ARRAY)) {
						c = readInternalArray(reader, e, 2);
					}
				}
			}
		}
		afterArray(param, c);
		param.children = childrenParam.toArray(new ContentDescriptionType.Parameter[0]);
		return param;
	}

	/**
	 * Return the string element of a Return element. This read the event until the end tag.
	 * It could have been the same a readParameter with an additional param, idk
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static ContentDescriptionType.Parameter readReturn(XMLEventReader reader, XMLEvent e) 
			throws XMLStreamException {
		ContentDescriptionType.Parameter param = new ContentDescriptionType.Parameter();
		ArrayList<ContentDescriptionType.Parameter> childrenParam = 
				new ArrayList<ContentDescriptionType.Parameter>();
		Iterator iterator = ((StartElement)e).getAttributes();
		ContentDescriptionType.Parameter c=null;

		while (reader.hasNext() && !isEndElementof(e, XML_TAG_METHOD_RETURN)) {
			e = reader.nextEvent();
			if (isStartElementof(e,XML_TAG_METHOD_PURPOSE) ) {
				param.description = readPurpose(reader, e);
			}
			else if (isStartElementof(e, XML_TAG_TYPE)) {
				iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("name")) {
						param.type = attribute.getValue();
					}
					if(name.toString ().equals ("fullname")) {
						param.fullType = attribute.getValue();
					}
				}
			}
			else if (isStartElementof(e, "param")) {
				ContentDescriptionType.Parameter child = new ContentDescriptionType.Parameter();
				iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("name")) {
						child.type = attribute.getValue();
					}
					if(name.toString ().equals ("fullname")) {
						child.fullType = attribute.getValue();
					}
				}
				childrenParam.add(child);
			}
			else if (isStartElementof(e, XML_TAG_ARRAY)) {
				param.isArray = true;
				param.depth=1;
				while ( reader.hasNext() && !isEndElementof(e, XML_TAG_ARRAY) ) {
					e = reader.nextEvent();
					if (isStartElementof(e, "component")) {
						iterator = ((StartElement)e).getAttributes();
						while (iterator.hasNext()) {
							Attribute attribute = (Attribute) iterator.next();
							QName name = attribute.getName();
							if(name.toString ().equals ("name")) {
								param.type = attribute.getValue();
							}
							if(name.toString ().equals ("fullname")) {
								param.fullType = attribute.getValue();
							}
						}
					}
					else if (isStartElementof(e, XML_TAG_ARRAY)) {
						c = readInternalArray(reader, e, 2);
					}
				}
			}
		}
		afterArray(param, c);
		param.children = childrenParam.toArray(new ContentDescriptionType.Parameter[0]);
		return param;
	}

	/**
	 * Return the string element of a type element. This read the event until the end tag.
	 * It could have been the same a readParameter with an additional param, idk
	 * The value of the field is stored in tmpString
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static ContentDescriptionType.Parameter readType(XMLEventReader reader, XMLEvent e) 
			throws XMLStreamException {
		ContentDescriptionType.Parameter param = new ContentDescriptionType.Parameter();
		ArrayList<ContentDescriptionType.Parameter> childrenParam = 
				new ArrayList<ContentDescriptionType.Parameter>();
		Iterator iterator = ((StartElement)e).getAttributes();
		ContentDescriptionType.Parameter c=null;

		while (iterator.hasNext()) {
			Attribute attribute = (Attribute) iterator.next();
			QName name = attribute.getName();
			if(name.toString ().equals ("name")) {
				param.type = attribute.getValue();
			}
			if(name.toString ().equals ("fullname")) {
				param.fullType = attribute.getValue();
			}
		}

		param.valueAsString = tmpValue;
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_TYPE)) {
			e = reader.nextEvent();
			if (isStartElementof(e, XML_TAG_METHOD_PURPOSE)) {
				param.description = readPurpose(reader, e);
			}
			else if (isStartElementof(e, "param")) {
				ContentDescriptionType.Parameter child = new ContentDescriptionType.Parameter();
				iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("name")) {
						child.type = attribute.getValue();
					}
					if(name.toString ().equals ("fullname")) {
						child.fullType = attribute.getValue();
					}
				}
				childrenParam.add(child);
			}
			else if (isStartElementof(e, XML_TAG_ARRAY)) {
				param.isArray = true;
				param.depth=1;
				while ( reader.hasNext() && !isEndElementof(e, XML_TAG_ARRAY) ) {
					e = reader.nextEvent();
					if (isStartElementof(e, "component")) {
						iterator = ((StartElement)e).getAttributes();
						while (iterator.hasNext()) {
							Attribute attribute = (Attribute) iterator.next();
							QName name = attribute.getName();
							if(name.toString ().equals ("name")) {
								param.type = attribute.getValue();
							}
							if(name.toString ().equals ("fullname")) {
								param.fullType = attribute.getValue();
							}
						}
					}
					else if (isStartElementof(e, XML_TAG_ARRAY)) {
						c = readInternalArray(reader, e, 2);
					}
				}
			}
		}
		afterArray(param, c);
		param.children = childrenParam.toArray(new ContentDescriptionType.Parameter[0]);
		return param;
	}

	private static ContentDescriptionType.Parameter readArray(XMLEventReader reader, XMLEvent e) 
			throws XMLStreamException {
		ContentDescriptionType.Parameter param = new ContentDescriptionType.Parameter();
		ArrayList<ContentDescriptionType.Parameter> childrenParam = 
				new ArrayList<ContentDescriptionType.Parameter>();
		ContentDescriptionType.Parameter c=null;
		Iterator iterator = ((StartElement)e).getAttributes();

		param.isArray = true;
		param.depth = 1;	// depth 1 for an array, 2 for and array of array [][], 3 ...

		while (iterator.hasNext()) { //should not happen in array
			Attribute attribute = (Attribute) iterator.next();
			QName name = attribute.getName();
			if(name.toString ().equals ("name")) {
				param.type = attribute.getValue();
			}
			if(name.toString ().equals ("fullname")) {
				param.fullType = attribute.getValue();
			}
		}

		param.valueAsString = tmpValue;
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_ARRAY)) {
			e = reader.nextEvent();
			if (isStartElementof(e, XML_TAG_METHOD_PURPOSE)) {
				param.description = readPurpose(reader, e);
			}
			else if (isStartElementof(e, "component")) {
				iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("name")) {
						param.type = attribute.getValue();
					}
					if(name.toString ().equals ("fullname")) {
						param.fullType = attribute.getValue();
					}
				}
			}
			else if (isStartElementof(e, XML_TAG_ARRAY)) {
				c = readInternalArray(reader, e, 2);
			}
		}
		afterArray(param, c);
		param.children = childrenParam.toArray(new ContentDescriptionType.Parameter[0]);
		return param;
	}


	/**
	 * Push up the content parsed by the array internal reading to the parent Param
	 * @param param
	 * @param c
	 */
	private static void afterArray(ContentDescriptionType.Parameter param, ContentDescriptionType.Parameter c) {
		while (c!=null) {
			param.depth+=1;
			if (c.children[0]==null) {
				param.type=c.type;
				param.fullType=c.fullType;
			}
			c = c.children[0];
		}
	}


	private static ContentDescriptionType.Parameter readInternalArray(XMLEventReader reader, XMLEvent e, int depth) 
			throws XMLStreamException {
		ContentDescriptionType.Parameter param = new ContentDescriptionType.Parameter();
		Iterator iterator;
		ContentDescriptionType.Parameter c=null;
		param.isArray = true;
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_ARRAY)) {
			e = reader.nextEvent();
			if (isStartElementof(e, "component")) {
				iterator = ((StartElement)e).getAttributes();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("name")) {
						param.type = attribute.getValue();
					}
					if(name.toString ().equals ("fullname")) {
						param.fullType = attribute.getValue();
					}
				}
			}
			else if (isStartElementof(e, XML_TAG_ARRAY)) {
				c = readInternalArray(reader, e, depth+1);
			}
		}
		param.children=new ContentDescriptionType.Parameter[] {c};
		return param;
	}


	/**
	 * Return the string element of a supertype element. This read the event until the end tag.
	 * It could have been the same a readParameter with an additional param, idk
	 * The value of the field is stored in tmpString
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static ContentDescriptionType readSuperClass(XMLEventReader reader, XMLEvent e) 
			throws XMLStreamException {
		ContentDescriptionType clazz = new ContentDescriptionClass();
		Iterator iterator;
		ContentDescriptionType tmpsuperclass;		

		while (reader.hasNext() && !isEndElementof(e, XML_TAG_CLASS_SUPERCLASS)) {
			e = reader.nextEvent();
			if (isStartElementof(e, XML_TAG_METHOD_PURPOSE)) {
				clazz.addTDescription(readPurpose(reader, e));
			}
			else if (isStartElementof(e,  XML_TAG_TYPE)) {
				Parameter p = readType(reader, e);
				clazz.parsePackage(p.fullType);
				clazz.setTName(p.type);
			}

			else if (isStartElementof(e,  XML_TAG_CLASS_INTERFACE)) {
				iterator = ((StartElement)e).getAttributes();
				tmpsuperclass = new ContentDescriptionClass();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("fullname")) {
						tmpsuperclass.parsePackage(attribute.getValue());
					}
					else if(name.toString ().equals ("name")) {
						tmpsuperclass.setTName(attribute.getValue());
					}
				}
				clazz.addTSuperClass(tmpsuperclass);
				while (reader.hasNext() && !isEndElementof(e, XML_TAG_CLASS_INTERFACE)) {
					e = reader.nextEvent();
				}
			}

			else if (isStartElementof(e,  XML_TAG_CLASS_SUPERCLASS)) {
				tmpsuperclass = readSuperClass(reader, e);
				clazz.addTSuperClass(tmpsuperclass);
			}
		}
		return clazz;
	}
	
	
	/**
	 * Return the tpye content of an interface element. This read the event until the end tag.
	 * It could have been the same a readParameter with an additional param, idk
	 * The value of the field is stored in tmpString
	 * @param e
	 * @return
	 * @throws XMLStreamException 
	 */
	private static ContentDescriptionType readInterfaces(XMLEventReader reader, XMLEvent e) 
			throws XMLStreamException {
		ContentDescriptionType clazz = new ContentDescriptionClass();
		Iterator iterator;
		ContentDescriptionType tmpsuperclass;		

		while (reader.hasNext() && !isEndElementof(e, XML_TAG_CLASS_INTERFACES)) {
			e = reader.nextEvent();
			if (isStartElementof(e, XML_TAG_INTERFACEREF)) {
				iterator = ((StartElement)e).getAttributes();
				tmpsuperclass = new ContentDescriptionClass();
				while (iterator.hasNext()) {
					Attribute attribute = (Attribute) iterator.next();
					QName name = attribute.getName();
					if(name.toString ().equals ("fullname")) {
						tmpsuperclass.parsePackage(attribute.getValue());
					}
					else if(name.toString ().equals ("name")) {
						tmpsuperclass.setTName(attribute.getValue());
					}
				}
				clazz.addTSuperClass(tmpsuperclass);
				while (reader.hasNext() && !isEndElementof(e, XML_TAG_CLASS_INTERFACES)) {
					e = reader.nextEvent();
				}
			}
		}
		return clazz;
	}

	protected static void loadContent (ContentDescriptionMethod content, StartElement e) {
		Iterator iterator = e.getAttributes();
		while (iterator.hasNext()) {
			Attribute attribute = (Attribute) iterator.next();
			QName name = attribute.getName();
			if(name.toString ().equals ("static"))			content.setTStatic (Boolean.valueOf( attribute.getValue()));
			if(name.toString ().equals ("access"))			content.setTVisibility (attribute.getValue());
			if(name.toString ().equals ("name"))			content.setTName (attribute.getValue());
			if(name.toString ().equals ("fullsig"))			content.parsePackage (attribute.getValue());
			if(name.toString ().equals ("final"))			content.setTFinal (Boolean.valueOf( attribute.getValue()));
		}
	}

	protected static void loadContent (ContentDescriptionField content, StartElement e) {
		Iterator iterator = e.getAttributes();
		while (iterator.hasNext()) {
			Attribute attribute = (Attribute) iterator.next();
			QName name = attribute.getName();
			if(name.toString ().equals ("static"))			content.setTStatic (Boolean.valueOf( attribute.getValue()));
			if(name.toString ().equals ("access"))			content.setTVisibility (attribute.getValue());
			if(name.toString ().equals ("name"))			content.setTName (attribute.getValue());
			if(name.toString ().equals ("final"))			content.setTFinal (Boolean.valueOf( attribute.getValue()));
			if(name.toString ().equals ("value"))			tmpValue = attribute.getValue();
		}
	}

	protected static void loadContent (ContentDescriptionClass content, StartElement e) {
		Iterator iterator = e.getAttributes();
		while (iterator.hasNext()) {
			Attribute attribute = (Attribute) iterator.next();
			QName name = attribute.getName();
			if(name.toString ().equals ("access"))			content.setTVisibility (attribute.getValue());
			if(name.toString ().equals ("fullname"))		content.parsePackage (attribute.getValue());
			if(name.toString ().equals ("package"))			content.setTPackage(attribute.getValue());
			if(name.toString ().equals ("abstract"))		content.setTAbstract(Boolean.valueOf( attribute.getValue()));
			if(name.toString ().equals ("static"))			content.setTStatic (Boolean.valueOf( attribute.getValue()));
			if(name.toString ().equals ("type"))			content.setTName (attribute.getValue());
		}
	}

	private final static String getStartElementName(XMLEvent e) {
		return e.asStartElement().getName().getLocalPart();
	}

	private final static String getEndElementName(XMLEvent e) {
		return e.asEndElement().getName().getLocalPart();
	}

	private final static boolean isStartElementMethod(XMLEvent e) {
		if(getStartElementName(e).equals(XML_TAG_METHOD)) {
			int size = 0;
			Iterator<Attribute> attr = e.asStartElement().getAttributes();
			while (attr.hasNext()) {
				size++;
				attr.next();
			}
			if (size > 1) {
				emptyMethod = false;
				return true;
			}
			emptyMethod = true;
		}
		return false;
	}

	private final static boolean isEndElementMethod(XMLEvent e) {
		if(getEndElementName(e).equals(XML_TAG_METHOD) && !emptyMethod) return true;
		return false;
	}

	private final static boolean isStartElementField(XMLEvent e) {
		if(getStartElementName(e).equals(XML_TAG_FIELD)) {
			int size = 0;
			Iterator<Attribute> attr = e.asStartElement().getAttributes();
			while (attr.hasNext()) {
				size++;
				attr.next();
			}
			if (size > 1) {
				emptyField = false;
				return true;
			}
			emptyField = true;
		}
		return false;
	}

	private final static boolean isEndElementField(XMLEvent e) {
		if(getEndElementName(e).equals(XML_TAG_FIELD) && !emptyField) return true;
		return false;
	}

	private final static boolean isStartElementClass(XMLEvent e) {
		if(getStartElementName(e).equals(XML_TAG_CLASS)) {
			int size = 0;
			staticOrprivateClass = false;
			Iterator<Attribute> attr = e.asStartElement().getAttributes();
			while (attr.hasNext()) {			
				size++;
				Attribute attribute = (Attribute) attr.next();
				// remove static classes that appear in double in the xmldoclet
				QName name = attribute.getName();
				if(name.toString ().equals ("static") || 
					( name.toString ().equals ("access") && !attribute.getValue().equals("public"))
				) {
					staticOrprivateClass = true;
					return true;
				}
			}
			if (size > 1) {
				emptyClass = false;
				return true;
			}
			emptyClass = true;
		}
		return false;
	}
	
	private final static boolean isEndElementClass(XMLEvent e) {
		if(getEndElementName(e).equals(XML_TAG_CLASS) && !emptyClass) return true;
		return false;
	}
	
	private final static boolean isStartElementConstructor(XMLEvent e) {
		if(getStartElementName(e).equals(XML_TAG_CLASS_CONSTRUCTOR)) return true;
		return false;
	}
	
	private final static boolean isEndElementConstructor(XMLEvent e) {
		if(getEndElementName(e).equals(XML_TAG_CLASS_CONSTRUCTOR) ) return true;
		return false;
	}


	private final static boolean isEndElementof(XMLEvent e, String tag) {
		return e.isEndElement() && getEndElementName(e).equals(tag);
	}

	private final static boolean isStartElementof(XMLEvent e, String tag) {
		return e.isStartElement() && getStartElementName(e).equals(tag);
	}

	private static int checkTypeBasedOnElement(XMLEvent e, ContentDescriptionType t) throws Exception {
		String elemType = getEndElementName(e);
		switch(elemType) {
		case XML_TAG_METHOD:
			if (t instanceof ContentDescriptionMethod) {
				return METHOD;
			}
			break;
		case XML_TAG_CLASS:
			if (t instanceof ContentDescriptionClass) {
				return CLASS;
			}
			break;
		case XML_TAG_FIELD:
			if (t instanceof ContentDescriptionField) {
				return FIELD;
			}
			break;
		case XML_TAG_CLASS_CONSTRUCTOR:
			if (t instanceof ContentDescriptionConstructor) {
				return CONSTRUCTOR;
			}
			break;
		}
	
		throw new Exception("Incorrect xml syntax.");
	}

	protected static void discardStaticOrPrivateClass(
			XMLEventReader reader, XMLEvent e) throws XMLStreamException {
		while (reader.hasNext() && !isEndElementof(e, XML_TAG_CLASS)) {
			e = reader.nextEvent();
		}
		staticOrprivateClass=false;
	}
	
}
