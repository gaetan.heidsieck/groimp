module grammar {
	exports de.grogra.grammar;

	requires utilities;
	requires xl.core;
	requires antlr;
	requires java.xml;
	
	opens de.grogra.grammar to utilities;
}
