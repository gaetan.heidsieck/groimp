/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.vecmath.geom;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

import de.grogra.vecmath.Math2;


/**
 * @author Ole Kniemeyer
 */
public class GridClonerUnion extends OctreeUnion
{
	int xCount;
	int yCount;
	Vector3d dx;
	Vector3d dy;
	Matrix4d globalToGrid;
	Point3d gridBBoxMin;
	Point3d gridBBoxMax;
	Point3d clonerBBoxMin;
	Point3d clonerBBoxMax;

	
	public GridClonerUnion(int xc, double xd, int yc, double yd, Matrix4d t)
	{
		xCount = xc;
		yCount = yc;
		dx = new Vector3d();
		dy = new Vector3d();
		Matrix4d scale = new Matrix4d();
		scale.m00 = xd;
		scale.m11 = yd;
		scale.m22 = 1;
		scale.m33 = 1;
		Matrix4d m = new Matrix4d(t);
		m.mul(scale);
		dx = new Vector3d(m.m00, m.m10, m.m20);
		dy = new Vector3d(m.m01, m.m11, m.m21);
		m.invert();
		globalToGrid = m;
	}

	@Override
	public void initialize (int maxDepth, int minObjects, CellIterator iterator)
	{
		super.initialize (maxDepth, minObjects, iterator);
		// Compute the bounding box in the grid coordinate system.
		Point3d min = new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
		Point3d max = new Point3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
		Point3d tmp = new Point3d();
		Point3d omin = getOctree().getMin();
		Point3d omax = getOctree().getMax();
		for (int x = 0; x <= 1; ++x)
		{
			for (int y = 0; y <= 1; ++y)
			{
				for (int z = 0; z <= 1; ++z)
				{
					tmp.set((x > 0) ? omax.x : omin.x, (y > 0) ? omax.y : omin.y, (z > 0) ? omax.z : omin.z);
					globalToGrid.transform(tmp);
					Math2.min(min, tmp);
					Math2.max(max, tmp);
				}
			}
		}
		double eps = min.distance(max) * 1e-10;
		tmp.set(eps, eps, eps);
		min.sub(tmp);
		max.add(tmp);
		gridBBoxMin = min;
		gridBBoxMax = max;

		clonerBBoxMin = new Point3d(omin);
		tmp.scaleAdd(xCount - 1, dx, omin);
		clonerBBoxMin.min(tmp);
		tmp.scaleAdd(yCount - 1, dy, omin);
		clonerBBoxMin.min(tmp);
		tmp.scaleAdd(xCount - 1, dx, tmp);
		clonerBBoxMin.min(tmp);

		clonerBBoxMax = new Point3d(omax);
		tmp.scaleAdd(xCount - 1, dx, omax);
		clonerBBoxMax.max(tmp);
		tmp.scaleAdd(yCount - 1, dy, omax);
		clonerBBoxMax.max(tmp);
		tmp.scaleAdd(xCount - 1, dx, tmp);
		clonerBBoxMax.max(tmp);
	}

	public boolean contains (Tuple3d point, boolean open)
	{
		Point3d tmp = new Point3d(point);
		globalToGrid.transform(tmp);
		if ((tmp.z > gridBBoxMax.z) || (tmp.z < gridBBoxMin.z))
			return false;
		// Now we have to check if tmp is in any of the bounding boxes [gridBBoxMin, gridBBoxMax] + (i, j, 0) of the clones.
		// So find i such that min.x <= tmp.x - i <= max.x, in other words tmp.x-max.x <= i <= tmp.x-min.y
		double i = Math.max(Math.ceil(tmp.x - gridBBoxMax.x), 0);
		double iEnd = Math.min(tmp.x - gridBBoxMin.x, xCount-1);
		// Same for j.
		double jStart = Math.max(Math.ceil(tmp.y - gridBBoxMax.y), 0);
		double jEnd = Math.min(tmp.y - gridBBoxMin.y, yCount-1);
		while (i <= iEnd)
		{
			double j = jStart;
			while (j <= jEnd)
			{
				tmp.scaleAdd(-i, dx, point);
				tmp.scaleAdd(-j, dy, tmp);
				// globalToGrid.transform(tmp);
				// if (Math2.lessThanOrEqual(gridBBoxMin, tmp) && Math2.lessThanOrEqual(tmp,  gridBBoxMax))
				//	return true;
				if (super.contains(tmp, open))
					return true;
				j += 1;			
			}
			i += 1;			
		}
		return false;
	}

	public void getExtent (Tuple3d min, Tuple3d max, Variables temp)
	{
		min.set(clonerBBoxMin);
		max.set(clonerBBoxMax);
	}

	public boolean computeIntersections (Line line, int which, IntersectionList list, Intersection excludeStart, Intersection excludeEnd)
	{
		Point3d origin = list.tmpPoint0;
		Vector3d dir = list.tmpVector0;
		Vector3d origLineOrigin = new Vector3d(line.origin);
		double origLineEnd = line.end;
		Math2.transformPoint(globalToGrid, origLineOrigin, origin);
		globalToGrid.transform(line.direction, dir);
		
		try
		{
			// At first intersect the line given by (origin, dir) with the minimum and maximum z coordinates,
			// i.e., gridBBoxMin.z <= origin.z + t * dir.z <= gridBBoxMax.z

			double t;
			if (dir.z == 0.0d)
			{
				if ((origin.z < gridBBoxMin.z) || (origin.z > gridBBoxMax.z))
					return false;
				t = line.start;
			}
			else
			{
				double iz = 1.0d / dir.z;
				double tMax;
				if (iz < 0)
				{
					tMax = (gridBBoxMin.z - origin.z) * iz;
					t = (gridBBoxMax.z - origin.z) * iz;
				}
				else
				{
					t = (gridBBoxMin.z - origin.z) * iz;
					tMax = (gridBBoxMax.z - origin.z) * iz;
				}
				if (t > line.end)
					return false;
				if (tMax < line.start)
					return false;
				if (tMax < line.end)
					line.end = tMax;
				if (t < line.start)
					t = line.start;
			}

			// Then intersect the line with the x/y extent of the bounding box.

			int atLine = -1; // 0 when t intersects a line in x-direction, 1 for y-direction

			double idx = (dir.x == 0.0d) ? 0.0d : 1.0d / dir.x;
			double idy = (dir.y == 0.0d) ? 0.0d : 1.0d / dir.y;
			for (int k = 0; k < 2; ++k)
			{
				double ik = (k == 0) ? idx : idy;
				double ok = origin.get(k);
				double mink = gridBBoxMin.get(k);
				double maxk = gridBBoxMax.get(k) + (((k == 0) ? xCount : yCount) - 1);
				if (ik == 0.0d)
				{
					if ((ok < mink) || (ok > maxk))
						return false;
				}
				else
				{
					double tMin;
					double tMax;
					if (ik < 0)
					{
						tMax = (mink - ok) * ik;
						tMin = (maxk - ok) * ik;
					}
					else
					{
						tMin = (mink - ok) * ik;
						tMax = (maxk - ok) * ik;
					}
					if (tMin > line.end)
						return false;
					if (tMax < t)
						return false;
					if (tMin > t)
					{
						t = tMin;
						atLine = k;
					}
					if (tMax < line.end)
						line.end = tMax;
				}
			}
			
			// Prepare intersection data.

			// initial size of list
			int intersectionStart = list.size;

			int cloneCount = xCount * yCount;
	
			// For each clone add an index. This will be set to the index of the first intersection.
			int index = list.ienter (cloneCount);
			// For each clone add an end index. This will be set to the index behind the last intersection.
			int end = list.ienter (cloneCount);
			// Initially, set all indices to -1. This is used below to see if a clone has already been handled.
			for (int k = index; k < list.getISize(); ++k)
			{
				list.istack[k] = -1;
			}

			try
			{
				boolean inside = false;
		
				// Now see in which clone bounding boxes the line point at t lies.  
				// This is the same algorithm as in the contains() method.
				Point3d tmp = list.tmpPoint1;
				tmp.scaleAdd(t, dir, origin);
				int iStart, iEnd, jStart, jEnd;
				double d;
				d = tmp.x - gridBBoxMax.x;
				if ((atLine == 0) && (idx < 0))
					iStart = (int) Math.round(d);
				else
					iStart = (int) Math.ceil(d);
				d = tmp.x - gridBBoxMin.x;
				if ((atLine == 0) && (idx > 0))
					iEnd = (int) Math.round(d);
				else
					iEnd = (int) Math.floor(d);
				d = tmp.y - gridBBoxMax.y;
				if ((atLine == 1) && (idy < 0))
					jStart = (int) Math.round(d);
				else
					jStart = (int) Math.ceil(d);
				d = tmp.y - gridBBoxMin.y;
				if ((atLine == 1) && (idy > 0))
					jEnd = (int) Math.round(d);
				else
					jEnd = (int) Math.floor(d);

				int i = Math.max(iStart, 0);
				while ((i <= iEnd) && (i < xCount))
				{
					int j = Math.max(jStart, 0);
					while ((j <= jEnd) && (j < yCount))
					{
						// Line point at t is within bounding box of clone (i,j).
						int k = i * yCount + j;
						int begin = list.size;
						list.istack[index + k] = begin;
						line.origin.scaleAdd(-i, dx, origLineOrigin);
						line.origin.scaleAdd(-j, dy, line.origin);
						inside |= super.computeIntersections (line, which, list, excludeStart, excludeEnd);
						list.istack[end + k] = list.size;
						for (k = begin; k < list.size; ++k)
						{
							list.elements[k].multiplicity *= cloneCount;
							list.elements[k].getLocalPoint(); // This caches the intersection point. Caching at a later time would compute the wrong point as line.origin will be reset to its original value.
						}
						if ((which != Intersection.ALL) && (list.size != begin))
						{
							if (which == Intersection.ANY)
							{
								return inside;
							}
							line.end = list.elements[begin].parameter;
						}
						j += 1;			
					}
					i += 1;			
				}
			
				// The clones whose bounding boxes contain t have already been checked. Now we have to walk along the line
				// towards line.end to see if we cross further clone bounding boxes. Let's assume the line direction
				// (given by dir in grid coordinates) is in the first quadrant, then we'll reach clones in the order of
				// increasing indices. Given that t has already been checked, we have to proceed until we reach the next
				// intersection with one of the conditions origin.w + t2 * dir.w == gridBBoxMin.w + i (where w is either x or y).

				// We start at the clone (i,j) = (iStart,jStart). Then we have to see whether the line intersects 
				// (i+1,j) or (i,j+1) at first. This is taken as next clone to test, and (i,j) is updated to the next clone.
				// Because clone bounding boxes may overlap, we also have to check for each clone its neighboring clones (i,j+m)
				// if we came from the (i+1,j)-case or (i+m,j) in the (i,j+1)-case.
				// For a line direction in one of the other quadrants, the same holds correspondingly.

				// Check quadrant.
				i = (idx >= 0) ? iEnd : iStart;
				int j = (idy >= 0) ? jEnd : jStart;

				while (true)
				{
					// Values might have been overwritten by nested calls to computeIntersections.
					Math2.transformPoint(globalToGrid, origLineOrigin, origin);
					globalToGrid.transform(line.direction, dir);

					atLine = 0;
					int i2;
					// Find next bounding box.
					if (idx == 0.0d)
					{
						i2 = i;
						t = Double.POSITIVE_INFINITY;
					}
					else if (idx > 0)
					{
						i2 = i + 1;
						t = (i2 + gridBBoxMin.x - origin.x) * idx;
					}
					else
					{
						i2 = i - 1;
						t = (i2 + gridBBoxMax.x - origin.x) * idx;
					}
					int j2;
					if (idy == 0.0d)
					{
						j2 = j;
					}
					else
					{
						double t2;
						if (idy > 0)
						{
							j2 = j + 1;
							t2 = (j2 + gridBBoxMin.y - origin.y) * idy;
						}
						else
						{
							j2 = j - 1;
							t2 = (j2 + gridBBoxMax.y - origin.y) * idy;
						}
						if (t < t2)
						{
							j2 = j;						
						}
						else
						{
							i2 = i;
							t = t2;
							atLine = 1;
						}
					}
					if (t > line.end)
						break;
					i = i2;
					j = j2;
					tmp.scaleAdd(t, dir, origin);
					// Line point at t is within bounding box of clone (i,j).
					// Also overlapping clones (i+m,j) or (i,j+m) might have been entered.
					int mMin;
					int mMax;
					if (atLine == 0)
					{
						if ((i < 0) || (i >= xCount))
							continue;
						mMin = Math.max(0, (int) Math.ceil(tmp.y - gridBBoxMax.y));
						mMax = Math.min(yCount-1, (int) Math.floor(tmp.y - gridBBoxMin.y));
					}
					else
					{
						if ((j < 0) || (j >= yCount))
							continue;
						mMin = Math.max(0, (int) Math.ceil(tmp.x - gridBBoxMax.x));
						mMax = Math.min(xCount-1, (int) Math.floor(tmp.x - gridBBoxMin.x));
					}
					int ii = i;
					int jj = j;
					for (int m = mMin; m <= mMax; ++m)
					{
						if (atLine == 0)
						{
							jj = m;
						}
						else
						{
							ii = m;
						}
						int k = ii * yCount + jj;
						if (list.istack[index + k] < 0)
						{
							// Copy code from above.
							// Intersections for clone (ii,jj) haven't been computed yet.
							int begin = list.size;
							list.istack[index + k] = begin;
							line.origin.scaleAdd(-ii, dx, origLineOrigin);
							line.origin.scaleAdd(-jj, dy, line.origin);
							inside |= super.computeIntersections (line, which, list, excludeStart, excludeEnd);
							list.istack[end + k] = list.size;
							for (k = begin; k < list.size; ++k)
							{
								list.elements[k].multiplicity *= cloneCount;
								list.elements[k].getLocalPoint(); // This caches the intersection point. Caching at a later time would compute the wrong point as line.origin will be reset to its original value.
							}
							if ((which != Intersection.ALL) && (list.size != begin))
							{
								if (which == Intersection.ANY)
								{
									return inside;
								}
								line.end = list.elements[begin].parameter;
							}
						}
					}
				}

				sortIntersections (which == Intersection.ALL, list, intersectionStart, cloneCount, index, end);
				return inside;
			}
			finally
			{
				list.setISize(index);
			}
		}
		finally
		{
			line.origin.set(origLineOrigin);
			line.end = origLineEnd;
		}
	}

	public boolean boxContainsBoundary (BoundingBox box, Tuple3d center, double radius, Variables temp)
	{
		// boxes intersect
		return Math2.lessThanOrEqual(box.min, clonerBBoxMax) && Math2.lessThanOrEqual(clonerBBoxMin, box.max);
	}
}
