module vecmath {
	exports javax.vecmath;
	exports de.grogra.vecmath;
	exports de.grogra.vecmath.geom;

	requires xl.core;
	requires java.desktop;
}