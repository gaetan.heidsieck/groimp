package de.grogra.xl.query;

/**
 * This class represents exceptions thrown by QueryStateMultiScale 
 * 
 * This class is part of the extension of XL for multiscale modelling.
 * 
 * @since 18-04-2013
 * @author yong
 *
 */
public class QueryStateMultiScaleException extends Exception{

}
